<?php

namespace Core;

class Labs
{
    private $basePath = "../Labs";
    private $courseCode;

    public function __construct($courseCode) {
        $this->courseCode = $courseCode;
    }

    public static function getCoursesList() {
        return [
            [
                "name" => "Вычислительная математика",
                "code" => "Cmath",
                "files" => [
                    ["name" => "Книга по MATLAB, графики", "url" => "https://drive.google.com/open?id=0B_WvdP39zYv_QmplMGxJVm40ejQ"],
                    ["name" => "Книга по MATLAB, матрицы", "url" => "https://drive.google.com/open?id=0B_WvdP39zYv_UlVHRW9GTEhfeGs"],
                    ["name" => "Книга по MATLAB, интегрирование, нелинейные уравнения и системы, полиномы", "url" => "https://drive.google.com/file/d/0B_WvdP39zYv_cnppUzRncXplZ1U/view?usp=sharing"],
                    ["name" => "Книга по MATLAB, оптимизация, нелинейное программирование", "url" => "https://drive.google.com/file/d/0B_WvdP39zYv_MVdzQVRuVGFyZFU/view?usp=sharing"],
                    ["name" => "Книга по MATLAB, Обыкновенные дифференциальные уравнеия и системы", "url" => "https://drive.google.com/file/d/0B_WvdP39zYv_NW9tT0hMT3p0cHM/view?usp=sharing"],
                    ["name" => "Примеры геометрических преобразований при помощи матриц (en)", "url" => "https://en.wikipedia.org/wiki/Eigenvalues_and_eigenvectors#Eigenvalues_of_geometric_transformations"],
                    ["name" => "Пример статистической обработки результатов измерений", "url" => "/files/Cmath/stat_example.pdf"],
                    ["name" => "Необходимое ПО", "url" => "magnet:?xt=urn:btih:b3d666303ca8fed8b89597a9bf652edbebcdf049&dn=MATLAB+R2018a"],
                ],
                "interactive" => [
                    [
                        "name" => "Введение в программирование",
                        "matlab" => "",
                        "python" => "https://colab.research.google.com/drive/1hpy5rXCTjktj2UooJGH6b0IjMJLiaUZ3#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Зомби против человеков",
                        "python" => "https://colab.research.google.com/drive/1Z2EtgeLcSzPjk2giJIwVIQS3GxO56ij4#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Подозрительное моделирование короновируса",
                        "python" => "https://colab.research.google.com/drive/1WPgYTIldtAeN_fT2f-62O2oa-UxSctp_#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Графики",
                        "matlab" => "https://drive.google.com/uc?id=1AdETKOiOlXTX1_KnH6H-ssckrXGOGVCg&export=download",
                        "matlab_old" => "/notebooks/graphics.html",
                        "python" => "https://colab.research.google.com/drive/1ZbFSxQ49cmdp93QZOBqSh3w_BDFZcs4g#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Матрицы, векторы, СЛАУ",
                        "matlab" => "https://drive.google.com/uc?id=17JT9IcDvEpZxpth4y0T00Y20wSsHmlPi&export=download",
                        "matlab_old" => "/notebooks/matrix.html",
                        "python" => "https://colab.research.google.com/drive/15nkbnFSN4F4YvDOEeuQe3elDymaH0IVn#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Обработка результатов измерений одной величины",
                        "matlab" => "https://drive.google.com/uc?id=13Q0YBl8t9hcbLh7Oaka1kkyEv6lVYVSR&export=download",
                        "matlab_old" => "/notebooks/measurement.html",
                        "python" => "https://colab.research.google.com/drive/1IIzpnEexQ0rzm5I1rkY-vKZF21C7zh4l#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Аппроксимация, интерполяция",
                        "matlab" => "",
                        "matlab_old" => "",
                        "python" => "https://colab.research.google.com/drive/1cQNhV1aTFx98x2KkiaBv82ofgzqJ7qL_#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Интегрирование",
                        "matlab" => "",
                        "matlab_old" => "",
                        "python" => "https://colab.research.google.com/drive/1uncZbeJjYTqMrnwR5ZxSVYJB7LPxWxkj#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Бонус: нейросети",
                        "matlab" => "",
                        "python" => "https://colab.research.google.com/drive/1QK9eMywjsaf2Q4uROjxHSNVSDVGIMd6Y#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Бонус: Титаник",
                        "matlab" => "",
                        "python" => "https://colab.research.google.com/drive/187p9mfBPWXXEZOZNLGlA6DqmdvxE7m3i#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Бонус: TMDB - кассовые сборы фильмов",
                        "matlab" => "",
                        "python" => "https://colab.research.google.com/drive/1kE0MjzWd6RmebMcbAFbLmPSWgDXRccF1#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                ],
                "gradebooks" => [
                    //["group" => "П-21 (весна 2021)", "url" => "https://docs.google.com/spreadsheets/d/1tUnTp1WsJgPkIDmozrz6bXz4YnYkbHEiUWtG3rDLflE/edit?usp=sharing"],
                ],
            ],
            [
                "name" => "Компьютерное моделирование",
                "code" => "Cmod",
                "files" => [
                    ["name" => "Кафедральные заготовки программ на Matlab", "url" => "/files/Cmod/matlab_kaf.zip"],
                    ["name" => "[Видео] Статика: составление уравнений", "url" => "https://youtu.be/x408iLEZ6Xo"],
                    ["name" => "[Видео] Статика: метод декомпозиции", "url" => "https://youtu.be/rVWowYPz40E"],
                    ["name" => "Методическое пособие \"Компьютерное моделирование простых гидравлических систем\"", "url" => "/files/Cmod/hydraulics.pdf"],
                    ["name" => "Методическое пособие \"Статистическая обработка результатов активного эксперимента\"", "url" => "/files/Cmod/ae.pdf"],
                    ["name" => "[Видео, англ.] Отличный курс \"Process Improvement Using Data\" (тематика лаб 3, 4)", "url" => "https://www.youtube.com/playlist?list=PLHUnYbefLmeOPRuT1sukKmRyOVd4WSxJE"],
                    ["name" => "Методическое пособие \"Теория инженерного эксперимента\" (лаба 4)", "url" => "http://tstu.ru/book/elib2/pdf/2017/karpushkin.pdf"],
                    ["name" => "Лекции", "url" => "http://icm.muctr.ru/study/modelling/lectures.html"],
                    ["name" => "Необходимое ПО", "url" => "magnet:?xt=urn:btih:b3d666303ca8fed8b89597a9bf652edbebcdf049&dn=MATLAB+R2018a"],
                ],
                "interactive" => [
                    [
                        "name" => "Введение в программирование",
                        "matlab" => "",
                        "python" => "https://colab.research.google.com/drive/1hpy5rXCTjktj2UooJGH6b0IjMJLiaUZ3#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Зомби против человеков",
                        "python" => "https://colab.research.google.com/drive/1Z2EtgeLcSzPjk2giJIwVIQS3GxO56ij4#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Подозрительное моделирование короновируса",
                        "python" => "https://colab.research.google.com/drive/1WPgYTIldtAeN_fT2f-62O2oa-UxSctp_#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Моделирование теплообменника",
                        "python" => "https://colab.research.google.com/drive/1fnUycbWuMBhFxQ7Qd2dKh0fTy83eL6r9#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Решение систем уравнений и статическая гидравлическая система (Лаба №1)",
                        "matlab" => "https://drive.google.com/uc?id=1XlG6iKll1dIH6ghHjDG3zdrOZIRnkU3w&export=download",
                        "matlab_old" => "/notebooks/hydraulics.html",
                        "python" => "https://colab.research.google.com/drive/15nE7s12iou5I6QXsDKHrthkcTdt3YptS#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Дифференциальные уравнения и динамическая гидравлическая система (Лаба №2)",
                        "matlab" => "",
                        "matlab_old" => "",
                        "python" => "https://colab.research.google.com/drive/1pmwsEruwPcbL1bdVOjw9ZWm40jscfbRg#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Выбор вида модели (обработка данных пассивного эксперимента) (Лаба №3)",
                        "matlab" => "",
                        "matlab_old" => "",
                        "python" => "https://colab.research.google.com/drive/1st1rn5ahQOGrPH4iHrdSQPdE5jtVuqmD#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                    [
                        "name" => "Дизайн эксперимента (обработка данных активного эксперимента) (Лаба №4)",
                        "matlab" => "",
                        "matlab_old" => "",
                        "python" => "https://colab.research.google.com/drive/1Rx6_E3tL1ZpHDFjHJ9aDBFIwC-uNcRwM#forceEdit=true&offline=true&sandboxMode=true"
                    ],
                ],
                "gradebooks" => [
                    ["group" => "О-36 (весна 2020)", "url" => "https://docs.google.com/spreadsheets/d/1Jv8YqT1Unpd_MSk9WXd2BQh0D0xOiuj3ytWS5JtP1t8/edit?usp=sharing"],
                    ["group" => "О-37 (весна 2020)", "url" => "https://docs.google.com/spreadsheets/d/1gQuAcKV87jvTOdwop_-9EbsdTIjp1_SEstrSYTjPd0I/edit?usp=sharing"],
                ]
            ],
            [
                "name" => "Оптимизация химико-технологических процессов",
                "code" => "Optim",
                "files" => [
                    ["name" => "Бинго", "url" => "https://colab.research.google.com/drive/14IkD2swKAe2KjfvMzY1RX4qjAqPuSedr?usp=sharing"],
                    ["name" => "Быстрая экскурсия по синтаксису Matlab", "url" => "https://drive.google.com/open?id=133lULrrQDEbvRmyXIEcLC8V1TBbUbqn0egmSWt79HH8"],
                    ["name" => "Введение в Matlab", "url" => "https://docs.google.com/document/d/1jgc2CkpU5IgKrTjeoRwg3pIfR-BDFa1RvSkfrtLJuik/edit?usp=sharing"],
                    ["name" => "Методичка по Matlab", "url" => "https://drive.google.com/open?id=0B_WvdP39zYv_TGRxNUVXMnRfOUU"],
                    ["name" => "Вопросы к устному коллоквиуму", "url" => "https://drive.google.com/file/d/0B_WvdP39zYv_TjRMajVWX01WZW8/view?usp=sharing"],
                    ["name" => "Необходимое ПО", "url" => "magnet:?xt=urn:btih:b3d666303ca8fed8b89597a9bf652edbebcdf049&dn=MATLAB+R2018a"],
                    ["name" => "Небольшое описание по 2-й работе", "url" => "https://drive.google.com/open?id=0B_WvdP39zYv_bTEwd3NzXzNubXc"],
                    ["name" => "Небольшое описание по 3-й работе", "url" => "https://drive.google.com/open?id=0B_WvdP39zYv_RU5GSVNwU0FwRk0"],
                    ["name" => "Примеры программ (кафедральные, неразобранные)", "url" => "https://drive.google.com/open?id=0B_WvdP39zYv_NGQ1Q2tYWTJvVUk"],
                ],
                "gradebooks" => [
                    //["group" => "МО-25", "url" => "https://docs.google.com/spreadsheets/d/1r5hcnqQg-HYiDGnlrwhrvARxXAOkUITL6yO7b8iDMWo/edit?usp=sharing"],
                    //["group" => "МПР-20,25", "url" => "https://docs.google.com/spreadsheets/d/12w4pdliS-8E6JODDiLYopL_gcrnIpXsZRPNCzMBqDO0/edit?usp=sharing"],
                ]
            ],
            [
                "name" => "Информатика",
                "code" => "Inform",
                "files" => [
                    ["name" => "Необходимое ПО", "url" => "magnet:?xt=urn:btih:b3d666303ca8fed8b89597a9bf652edbebcdf049&dn=MATLAB+R2018a"],
                ],
                "gradebooks" => [
                    //["group" => "МТ-19", "url" => "https://docs.google.com/spreadsheets/d/1A3VwjQGzTZwN0nUIBM9cK8N3_3rkzAJIPeTWI4wgek4/edit?usp=sharing"],
                ]
            ],
            [
                "name" => "ИТ в научных исследованиях",
                "code" => "Research",
                "files" => [
                    ["name" => "Быстрая экскурсия по синтаксису Matlab", "url" => "https://drive.google.com/open?id=133lULrrQDEbvRmyXIEcLC8V1TBbUbqn0egmSWt79HH8"],
                    ["name" => "Введение в Matlab", "url" => "https://docs.google.com/document/d/1jgc2CkpU5IgKrTjeoRwg3pIfR-BDFa1RvSkfrtLJuik/edit?usp=sharing"],
                    ["name" => "Методичка по Matlab", "url" => "https://drive.google.com/open?id=0B_WvdP39zYv_TGRxNUVXMnRfOUU"],
                    ["name" => "Лекция 1", "url" => "https://drive.google.com/open?id=13IpWiuYJJlBJR0T9DQfrOM9lRjZu1eI4"],
                    ["name" => "Лекция 2", "url" => "https://drive.google.com/open?id=1tdO7ORB0oS7GvRDnjbNifWRHS75mLuPj"],
                    ["name" => "Заготовки программ", "url" => "https://drive.google.com/open?id=1XqkLn8M5y1_MG2NT1hSflwXwYwojDWT1"],
                    ["name" => "Необходимое ПО", "url" => "magnet:?xt=urn:btih:b3d666303ca8fed8b89597a9bf652edbebcdf049&dn=MATLAB+R2018a"],
                    ["name" => "Задачи для начинающих", "url" => "https://drive.google.com/open?id=18sJ6uEPnkPQkIK_B3V2nXNvVC-7rwUBBzNOa5ZGKgIA"],
                    ["name" => "Дополнительные задачи на программирование", "url" => "http://acmp.ru/index.asp?main=tasks"],
                ],
                "gradebooks" => [
                    //["group" => "МТ-19", "url" => "https://docs.google.com/spreadsheets/d/1nNYvqYU50lPHUVkLMldhhT75T-HXetRgYDh-UN_RLuA/edit?usp=sharing"],
                ]
            ]

        ];
    }

    public function getCourseConfig($courseCode) {
        $result = FALSE;
        foreach (self::getCoursesList() as $courseConfig) {
            if ($courseConfig['code'] == $courseCode) {
                $result = $courseConfig;
            }
        }

        return $result;
    }

    public function getLabList() {
        $labs = [];
        foreach (glob("{$this->basePath}/{$this->courseCode}/*.php") as $labConfigFileName) {
            $labConfig = require($labConfigFileName);
            $labCode = str_replace('.php', '', basename($labConfigFileName));
            $labs[$labCode] = $labConfig;
        }

        return $labs;
    }

    public function getLabConfig($labCode) {
        $labsConfigs = $this->getLabList();
        return $labsConfigs[$labCode];
    }

    private function getTask($taskConfig, $taskCode, $variant) {
        $taskText = $this->makeTaskText($taskConfig['text'], $taskConfig['vars'], $taskCode, $variant);
        return [
            "text" => $taskText,
            "code" => $taskCode,
        ];
    }

    public function getTaskCodeForSingle($labConfig, $variant) {
        if ($labConfig['type'] == 'multiple') {
            return false;
        }

        $maxTasks = count($labConfig["tasks"]);
        list($groupCode, ) = explode('-', $variant);
        $generator = new NumberGenerator($groupCode);
        $taskNum = $generator->getRoundRobinNum($groupCode, 0, $maxTasks);
        $taskCodes = array_keys($labConfig["tasks"]);
        $taskCode = $taskCodes[$taskNum];

        return $taskCode;
    }

    public function getLabTasks($labCode, $variant, array $labConfig = []) {
        if ( empty($labConfig) ) {
            $labConfig = require("{$this->basePath}/{$this->courseCode}/{$labCode}.php");
        }
        $tasks = [];

        if ($labConfig['type'] == 'multiple') {
            foreach ($labConfig["tasks"] as $taskCode => $taskConfig ) {
                $tasks[] = $this->getTask($taskConfig, $taskCode, $variant);
            }
        }
        else {
            $taskCode = $this->getTaskCodeForSingle($labConfig, $variant);
            $taskConfig = $labConfig["tasks"][$taskCode];
            $tasks[] = $this->getTask($taskConfig, $taskCode, $variant);
        }

        return $tasks;
    }

    private function isXYArray($data) {
        return is_array($data) && isset($data['x']) && (isset($data['y']) || isset($data['y0']));
    }

    private function isVarsArray($data) {
        if (!is_array($data)) {
            return false;
        }

        foreach (array_keys($data) as $k => $v) {
            if ($k !== $v) {
                return true;
            }
        }
        return false;
    }

    private function makeTaskText($textTemplate, $vars, $taskCode, $variant) {
        $replacementPairs = [];
        foreach ($vars as $varType => $varList) {
            $varsValues = $this->generateVars($varType, $varList, $taskCode, $variant);
            foreach ($varsValues as $varName => $varValue) {
                $strVarValue = $varValue;
                if ( $this->isXYArray($varValue) ) {
                    foreach ($varValue as $varSubName => $varNameValue) {
                        $replacementPairs[":{$varName}_".strtoupper($varSubName)] = implode(', ', $varNameValue);
                    }
                }
                elseif ( $this->isVarsArray($varValue) ) {
                    foreach ($varValue as $key => $value) {
                        $replacementPairs[":{$varName}_{$key}"] = implode(', ', $value);
                    }
                }
                else {
                    if ( is_array($varValue) ) {
                        $strVarValue = implode(', ', $varValue);
                    }
                    $replacementPairs[":{$varName}"] = $strVarValue;
                }
            }
        }

        $taskText = strtr($textTemplate, $replacementPairs);
        return $taskText;
    }

    public function getAllVars($vars, $taskCode, $variant) {
        $varValues = [];
        foreach ($vars as $varType => $varList) {
            $varsValues = $this->generateVars($varType, $varList, $taskCode, $variant);
            foreach ($varsValues as $varName => $varValue) {
                $strVarValue = $varValue;
                if ( $this->isXYArray($varValue) ) {
                    foreach ($varValue as $varSubName => $varNameValue) {
                        $varValues["{$varName}_".strtoupper($varSubName)] = "(".implode(', ', $varNameValue).")";
                    }
                }
                elseif ( $this->isVarsArray($varValue) ) {
                    foreach ($varValue as $key => $value) {
                        $varValues["{$varName}_{$key}"] = "(".implode(', ', $value).")";
                    }
                }
                else {
                    if ( is_array($varValue) ) {
                        $strVarValue = "(".implode(', ', $varValue).")";
                    }
                    $varValues[$varName] = $strVarValue;
                }
            }
        }

        return $varValues;
    }

    private function makeExecCommand($commandTemplate, $labConfig, $variant) {
        $vars = [];
        foreach ($labConfig["tasks"] as $taskCode => $taskConfig ) {
            foreach ($taskConfig['vars'] as $varType => $varList) {
                $varsValues = $this->generateVars($varType, $varList, $taskCode, $variant);
                $isString = strpos($varType, "str") === 0;
                foreach ($varsValues as $varName => $varValue) {
                    if ($this->isXYArray($varValue)) {
                        foreach (array_keys($varValue) as $varVariant) {
                            $matlabVarName = $varName."_".strtoupper($varVariant)."_".$taskCode;
                            $vars[]="global {$matlabVarName}";
                            $vars[]=$matlabVarName."=[".implode(', ', $varValue[$varVariant])."]";
                        }
                    }
                    elseif ($this->isVarsArray($varValue)) {
                        foreach ($varValue as $key=>$value) {
                            $matlabVarName = $varName."_".$key."_".$taskCode;
                            $vars[]="global {$matlabVarName}";
                            $vars[]=$matlabVarName."=[".implode(', ', $value)."]";
                        }
                    }
                    else {
                        if ($isString) {
                            $varValue = "'".$varValue."'";
                        }

                        if (is_array($varValue)) {
                            $varValue = "[".implode(', ', $varValue)."]";
                        }
                        $matlabVarName = $varName."_".$taskCode;
                        $vars[]="global {$matlabVarName}";
                        $vars[]=$matlabVarName."=".$varValue;
                    }
                }
            }
        }

        $command = strtr($commandTemplate, [
            ":vars" => implode(';', $vars),
            ":variant" => $variant,
            ":task" => $this->getTaskCodeForSingle($labConfig, $variant)
        ]);

        return $command;
    }

    private function execCalculation($labConfig, $variant) {
        $command = $this->makeExecCommand($labConfig['exec'], $labConfig, $variant);
        putenv('LANG=ru_RU.UTF-8');
        exec($command, $output, $exitvar);
        return $output;
    }

    private function parseVarTypeConfig($varType) {
        preg_match('#([a-z]+)\((.*?)\)#', $varType, $matches);
        $typeCode = strtolower($matches[1]);
        $typeConfigString = $matches[2];

        switch ($typeCode) {
            case 'int':
                list($min, $max) = explode('-', $typeConfigString);
                $typeConfig = ['min' => intval($min), 'max' => intval($max)];
            break;
            case 'float':
                list($range, $roundDigits) = explode(',', $typeConfigString);
                list($min, $max) = explode('-', $range);
                $typeConfig = ['min' => floatval($min), 'max' => floatval($max), 'digits' => intval($roundDigits)];
            break;
            case 'normal':
                list($range, $roundDigits, $count) = explode(',', $typeConfigString);
                list($min, $max) = explode('-', $range);
                $typeConfig = [
                    'min' => floatval($min),
                    'max' => floatval($max),
                    'digits' => intval($roundDigits),
                    'count' => intval($count)
                ];
            break;
            case 'func':
                list($func, $range, $roundDigits, $count) = explode(',', $typeConfigString);
                list($min, $max) = explode('-', $range);
                $typeConfig = [
                    'function' => $func,
                    'min' => floatval($min),
                    'max' => floatval($max),
                    'digits' => intval($roundDigits),
                    'count' => intval($count)
                ];
            break;
            case 'rndfunc':
                list($range, $roundDigits, $count) = explode(',', $typeConfigString);
                list($min, $max) = explode('-', $range);
                $typeConfig = [
                    'min' => floatval($min),
                    'max' => floatval($max),
                    'digits' => intval($roundDigits),
                    'count' => intval($count)
                ];
            break;
            case 'active':
                $typeConfig = [];
            break;
            case 'str':
                $variants = explode('|', $typeConfigString);
                $typeConfig = ['variants' => $variants];
            break;
        }

        $typeConfig['type'] = $typeCode;

        return $typeConfig;
    }

    private function generateVars($varType, $varNames, $taskCode, $variant) {
        list($groupCode, ) = explode('-', $variant);
        $typeConfig = $this->parseVarTypeConfig($varType);
        $generator = new NumberGenerator($variant);

        $vars = [];
        foreach ($varNames as $varName) {
            switch ($typeConfig['type']) {
                case 'int':
                    $vars[$varName] = $generator->getTaskCoefficient($taskCode, $varName, $typeConfig['min'], $typeConfig['max'], 0);
                break;
                case 'float':
                    $vars[$varName] = $generator->getTaskCoefficient($taskCode, $varName, $typeConfig['min'], $typeConfig['max'], $typeConfig['digits']);
                break;
                case 'normal':
                    $vars[$varName] = $generator->getNormalDistributedData(
                        $typeConfig['count'],
                        $taskCode,
                        $varName,
                        $typeConfig['min'],
                        $typeConfig['max'],
                        $typeConfig['digits']
                    );
                break;
                case 'func':
                    $vars[$varName] = $generator->getFunctionDistributedData(
                        $typeConfig['function'],
                        $typeConfig['count'],
                        $taskCode,
                        $varName,
                        $typeConfig['min'],
                        $typeConfig['max'],
                        $typeConfig['digits']
                    );
                    break;
                case 'rndfunc':
                    $vars[$varName] = $generator->getRandomFunctionDistributedData(
                        $typeConfig['count'],
                        $taskCode,
                        $varName,
                        $typeConfig['min'],
                        $typeConfig['max'],
                        $typeConfig['digits']
                    );
                break;
                case 'active':
                    $vars[$varName] = $generator->getActiveData();
                break;
                case 'str':
                    $vars[$varName] = $generator->getRandomListElement($taskCode, $varName, $typeConfig['variants']);
                break;
            }
        }

        return $vars;
    }

    private function removeMatlabHeader($matlabOutput) {
        $headerEndText = "academic, research, commercial, or other organizational use";
        $trailLinesNum = 1;
        $lastLineNum = 0;
        foreach ($matlabOutput as $lineNum => $line) {
            if (strpos($line, $headerEndText) !== false) {
                $lastLineNum = $lineNum;
                break;
            }
        }

        return array_slice($matlabOutput, $lastLineNum+$trailLinesNum);
    }

    private function extractAnswerFromMatlabOutput($matlabOutput, $answerCode) {
        $startLine = '=== '.$answerCode;
        $finishLine = '===';
        $result = false;

        $answerFound = false;
        $answerLines = [];
        foreach ($matlabOutput as $line) {
            if ($answerFound) {
                if ($line == $finishLine) {
                    break;
                }
                else {
                    $answerLines[] = $line;
                }
            }
            if ($line == $startLine) {
                $answerFound = true;
            }
        }

        if ($answerFound) {
            $result = implode("\n", $answerLines);
        }

        return $result;
    }

    private function getMatlabAnswer($resultOutput, $taskCode) {
        $progOutput = $this->removeMatlabHeader($resultOutput);
        if ($answerText = $this->extractAnswerFromMatlabOutput($progOutput, $taskCode)) {
            $answer = "<pre>{$answerText}</pre>";
        }
        else {
            $answer = "<pre>".implode("\n", $progOutput)."</pre>";
        }

        return $answer;
    }

    private function getGraphAnswer($answerConfig, $labCode, $variant, $taskCode) {
        if ($answerConfig['count'] && $answerConfig['count'] > 0) {
            $answer = "";
            for ($imageIndex = 1; $imageIndex <= $answerConfig['count']; $imageIndex++) {
                $imageUrl = "/img/ans/{$this->courseCode}/{$labCode}/{$variant}/{$taskCode}_{$imageIndex}.png";
                $answer .= "<p><a href=\"{$imageUrl}\" target=\"_blank\"><img src=\"{$imageUrl}\" height=\"300px\"></a></p>";
            }
        }
        else {
            $imageUrl = "/img/ans/{$this->courseCode}/{$labCode}/{$variant}/{$taskCode}.png";
            $answer = "<a href=\"{$imageUrl}\" target=\"_blank\"><img src=\"{$imageUrl}\" height=\"300px\" ></a>";
        }

        return $answer;
    }

    public function getAnswer($answerConfig, $resultOutput, $taskCode, $labCode, $variant) {
        switch ($answerConfig['type']) {
            case "matlab":
                $answer = $this->getMatlabAnswer($resultOutput, $taskCode);
            break;
            case "graph":
                $answer = $this->getGraphAnswer($answerConfig, $labCode, $variant, $taskCode);
            break;
            case "matlab_graph":
                $answer = $this->getMatlabAnswer($resultOutput, $taskCode);
                $answer.= $this->getGraphAnswer($answerConfig, $labCode, $variant, $taskCode);
            break;
            default:
                $answer = "Неизвестный тип ответа";
                break;
        };

        return [
            "code" => $taskCode,
            "text" => $answer,
            "debug" => implode("\n", $resultOutput),
        ];
    }

    public function getLabAnswers($labCode, $variant, array $labConfig = []) {
        if (empty($labConfig)) {
            $labConfig = require("{$this->basePath}/{$this->courseCode}/{$labCode}.php");
        }

        $resultOutput = $this->execCalculation($labConfig, $variant);
        $answers = [];

        if ($labConfig['type'] == 'multiple') {
            foreach ($labConfig["tasks"] as $taskCode => $taskConfig ) {
                $answerConfig = $taskConfig['answer'];
                $answers[] = $this->getAnswer($answerConfig, $resultOutput, $taskCode, $labCode, $variant);
            }
        }
        else {
            $taskCode = $this->getTaskCodeForSingle($labConfig, $variant);
            $answerConfig = $labConfig["tasks"][$taskCode]['answer'];
            $answers[] = $this->getAnswer($answerConfig, $resultOutput, $taskCode, $labCode, $variant);
        }

        return $answers;
    }

}