<?php

namespace Core;

use gburtini\Distributions\Normal;

class NumberGenerator
{
    private $variant;
    private $functionConfig;
    private $activeDataCollection;

    public function __construct($variant) {
        $this->variant = $variant;
        /*
         * pass_1+ 120-123
         * pass_2+ 127-123
         * pass_3+ 122-123
         * pass_4+ 126-123
         * pass_5+ 121-123
         */
        $this->functionConfig = [
            "pass_1" => [
                'coeffCount' => 2,
                'coeffRange' => [ 0=>[2, 5], 1=>[10, 100] ],
                'func' =>function ($x, array $a) {
                    return exp( $a[0]+$a[1]/$x );
                },
            ],
            "pass_2" => [
                'coeffCount' => 3,
                'coeffRange' => [ 0=>[4, 8], 1=>[2, 5], 2=>[0.01, 0.1] ],
                'func' =>function ($x, array $a) {
                    return exp( $a[0]-$a[1]/($a[2]+$x) );
                },
            ],
            "pass_3" => [
                'coeffCount' => 3,
                'coeffRange' => [ 0=>[2, 5], 1=>[0.001, 0.01], 2=>[0.0001, 0.0005] ],
                'func' =>function ($x, array $a) {
                    return exp( $a[0]-$a[1]*$x+$a[2]*pow($x,2) );
                },
            ],
            "pass_4" => [
                'coeffCount' => 4,
                'coeffRange' => [ 0=>[2, 5], 1=>[1, 10], 2=>[0.005, 0.01], 3=>[0.0001, 0.0005] ],
                'func' =>function ($x, array $a) {
                    return exp( $a[0]+$a[1]/$x+$a[2]*$x-$a[3]*log($x) );
                },
            ],
            "pass_5" => [
                'coeffCount' => 4,
                'coeffRange' => [ 0=>[2, 5], 1=>[0.01, 0.1], 2=>[0.0001, 0.001], 3=>[0.0000001, 0.000001] ],
                'func' =>function ($x, array $a) {
                    return exp( $a[0]-$a[1]*$x+$a[2]*pow($x,2)-$a[3]*pow($x,3));
                },
            ],
            "lin" => [
                'coeffCount' => 2,
                'coeffRange' => [0 => [0, 10], 1 => [0, 10]],
                'func' => function ($x, array $a) {
                    return $a[0] + $a[1] * $x;
                }
            ],
            "xexp" => [
                'coeffCount' => 2,
                'coeffRange' => [0 => [1, 2], 1 => [1, 2]],
                'func' => function ($x, array $a) {
                    return $a[0] * pow($x,2) * exp( -$a[1] * $x );
                }
            ],
            "react" => [
                'coeffCount' => 8,
                'coeffRange' => [
                    0 => [0.02, 0.026],
                    1 => [0.007, 0.008],
                    2 => [0.02, 0.025],
                    3 => [-0.19, -0.2],

                    4 => [11, 12],
                    5 => [3, 4],
                    6 => [1.5, 2],
                    7 => [40, 45],
                ],
                'func' => function ($x, array $a) {
                    return [
                        abs($a[3] + ($a[0] / ($a[1] * $x + $a[2]))),
                        abs($a[4] * $x / ($a[5] * pow($x, $a[6]) + $a[7]))
                    ];
                }
            ],
        ];

        $this->activeDataCollection = [
            [
                "T" =>  [   320,    340,    320,    340,  316.8,  343.2,    330,    330,    330,    330,    330,    330,    330,    330],
                "t" =>  [    50,     50,    100,    100,     75,     75,     42,    108,     75,     75,     75,     75,     75,     75],
                "Cp" => [0.1312, 0.0219, 0.0792, 0.0136,  0.129, 0.0127, 0.0609, 0.0307, 0.0409, 0.0394, 0.0395, 0.0339, 0.0413, 0.0417]
            ],
            [
                "T" =>  [   320,    340,    320,    340,  316.8,  343.2,    330,    330,    330,    330,    330,    330,    330,    330],
                "t" =>  [    50,     50,    100,    100,     75,     75,     42,    108,     75,     75,     75,     75,     75,     75],
                "Cp" => [0.1756, 0.0283, 0.1123, 0.0186,  0.177, 0.0168, 0.0803, 0.0434, 0.0563, 0.0547, 0.0565, 0.0545, 0.0543, 0.0544]
            ],
            [
                "T" =>  [   320,    340,    320,    340,  316.8,  343.2,    330,    330,    330,    330,    330,    330,    330,    330],
                "t" =>  [    50,     50,    100,    100,     75,     75,     42,    108,     75,     75,     75,     75,     75,     75],
                "Cp" => [0.1527, 0.0142, 0.0893, 0.0073, 0.1524, 0.0062, 0.0608, 0.0259, 0.0359, 0.0375, 0.0351, 0.0357, 0.0358, 0.0379]
            ],
            [
                "T" =>  [   320,    340,    320,    340,  316.8,  343.2,    330,    330,    330,    330,    330,    330,    330,    330],
                "t" =>  [    50,     50,    100,    100,     75,     75,     42,    108,     75,     75,     75,     75,     75,     75],
                "Cp" => [0.0504, 0.0026,  0.028, 0.0013, 0.0522,  0.001, 0.0149,  0.006, 0.0088, 0.0084, 0.0081, 0.0081, 0.0088, 0.0088]
            ],
            [
                "T" =>  [   320,    340,    320,    340,  316.8,  343.2,    330,    330,    330,    330,    330,    330,    330,    330],
                "t" =>  [    50,     50,    100,    100,     75,     75,     42,    108,     75,     75,     75,     75,     75,     75],
                "Cp" => [0.0373,  0.001,   0.02, 0.0008, 0.0399, 0.0006, 0.0099,  0.004, 0.0057, 0.0055, 0.0055, 0.0055, 0.0058, 0.0058]
            ],

        ];
    }

    private function numberInRange($seed, $rangeMin, $rangeMax) {
        srand($seed);
        $base = rand()/getrandmax();
        $number = $base * ($rangeMax - $rangeMin) + $rangeMin;

        return $number;
    }

    private function getSeed($string) {
        $seed = crc32($string);
        return $seed;
    }

    public function getRoundRobinNum($groupCode, $min, $max) {
        return $max == 0 ? 0 : $this->getSeed($groupCode) % $max + $min;
    }

    public function getTaskCoefficient($taskNumber, $coefficientNumber, $rangeMin, $rangeMax, $roundDigits = 2) {
        $seed = $this->getSeed( $this->variant.$taskNumber.$coefficientNumber );
        $rawNumber = $this->numberInRange($seed, $rangeMin, $rangeMax);
        return round($rawNumber, $roundDigits);
    }

    public function getBasicTaskCoefficinets($taskNumber, $count) {
        $coefficients = [];
        for ($index = 1; $index <= $count; $index++) {
            $coefficients[] = $this->getTaskCoefficient($taskNumber, $index, 1, 10, 0);
        }

        return $coefficients;
    }

    public function getBasicFuncCoefficinets($taskNumber, $funcName) {
        $seed = $taskNumber.$funcName;
        $funcConfig = $this->functionConfig[$funcName];
        $count = $funcConfig['coeffCount'];
        $coefficients = [];
        for ($index = 1; $index <= $count; $index++) {
            $min = $funcConfig['coeffRange'][$index-1][0];
            $max = $funcConfig['coeffRange'][$index-1][1];
            $coefficients[] = $this->getTaskCoefficient($seed, $index, $min, $max, 10);
        }

        return $coefficients;
    }

    public function getRandomListElement($taskNumber, $coefficientNumber, $list) {
        $index = $this->getTaskCoefficient($taskNumber, $coefficientNumber, 0, count($list)-1, 0);
        return $list[$index];
    }

    public function generateNormalDistributedData($count, $mean, $var, $roundDigits = 2) {
        $distribution = new Normal($mean, $var);

        $rands = [];
        foreach ($distribution->rands($count) as $rand) {
            $rands[] = round($rand, $roundDigits);
        };

        return $rands;
    }

    public function generateFunctionData($funcName, $a, $count, $xRangeMax = 100, $xRangeMin = 0, $noiseLevel = 0.1,
$roundDigits = 2) {
        $var = ($noiseLevel/$count)/2;
        $yNoise = $this->generateNormalDistributedData($count, $noiseLevel, $var, 4);
        $stepSize = ($xRangeMax-$xRangeMin)/$count;
        $xNoise = $this->generateNormalDistributedData($count, $stepSize, $stepSize*$noiseLevel, 4);

        $data = ['x' => []];
        for ($i = 0; $i < $count; $i++) {
            $x = $xRangeMin + $i*$stepSize+($xNoise[$i]-$stepSize);
            if ($x < $xRangeMin) {
                $x = $xRangeMin;
            }

            if ($x > $xRangeMax) {
                $x = $xRangeMax;
            }

            $y = $this->functionConfig[$funcName]['func']($x, $a);
            $nextY = $this->functionConfig[$funcName]['func']($x+$stepSize, $a);

            if (is_array($y)) {
                foreach ($y as $index => $yval) {
                    $yNoiseStep = ($noiseLevel - $yNoise[$i])*($nextY[$index]-$y[$index]);
                    $data['y'.$index][] = round($y[$index] + $yNoiseStep, $roundDigits);
                }
            }
            else {
                $yNoiseStep = ($noiseLevel - $yNoise[$i])*($nextY-$y);
                $y = $y + $yNoiseStep;
                $data['y'][] = round($y, $roundDigits);
            }
            $data['x'][] = round($x, $roundDigits);
        }

        return $data;
    }

    public function getNormalDistributedData($count, $taskNumber, $coefficientNumber, $rangeMin, $rangeMax, $roundDigits = 2) {
        $cachePath = '../tmp/Cmath-03-stat.txt';
        $savedData = false;
        try {
            $asArray = true;
            $data = json_decode(file_get_contents($cachePath), $asArray);
            if ( isset($data[$this->variant]) ) {
                $savedData = $data[$this->variant];
            }
        } catch (\Exception $exception) {
            $data = [];
        }

        if (!$savedData) {
            $mean = $this->getTaskCoefficient($taskNumber, $coefficientNumber, $rangeMin, $rangeMax, $roundDigits);
            $var =  $this->getTaskCoefficient($taskNumber, $coefficientNumber, 0, 2, 4);
            $rands = $this->generateNormalDistributedData($count, $mean, $var, $roundDigits);
            $data[$this->variant] = $rands;
            file_put_contents($cachePath, json_encode($data));
        }
        else {
            $rands = $savedData;
        }

        return $rands;
    }

    public function getRandomFunctionDistributedData($count, $taskNumber, $coefficientNumber, $rangeMin, $rangeMax, $roundDigits = 2) {
        $cachePath = '../tmp/Cmod-03-passive.txt';
        $savedData = false;
        try {
            $asArray = true;
            $data = json_decode(file_get_contents($cachePath), $asArray);
            if ( isset($data[$this->variant]) ) {
                $savedData = $data[$this->variant];
            }
        } catch (\Exception $exception) {
            $data = [];
        }

        if (!$savedData) {
            $cmod3LabFunctionCount = 5;
            $functionNum = intval($this->getTaskCoefficient($taskNumber, $coefficientNumber, 0, $cmod3LabFunctionCount - 1, 0));
            $functionName = array_keys($this->functionConfig)[ $functionNum ];
            $a = $this->getBasicFuncCoefficinets($taskNumber, $functionName);
            $noiseLevel = 0.1;
            $rands = $this->generateFunctionData($functionName, $a, $count, $rangeMax, $rangeMin, $noiseLevel, $roundDigits);
            $data[$this->variant] = $rands;
            file_put_contents($cachePath, json_encode($data));
        }
        else {
            $rands = $savedData;
        }

        return $rands;
    }

    public function getFunctionDistributedData($functionName, $count, $taskNumber, $coefficientNumber, $rangeMin, $rangeMax, $roundDigits = 2) {
        $cachePath = '../tmp/functionRands.txt';
        $savedData = false;
        try {
            $asArray = true;
            $data = json_decode(file_get_contents($cachePath), $asArray);
            if ( isset($data[$this->variant]) && isset($data[$this->variant][$taskNumber]) ) {
                $savedData = $data[$this->variant][$taskNumber];
            }
        } catch (\Exception $exception) {
            $data = [];
        }

        if (!$savedData) {
            $a = $this->getBasicFuncCoefficinets($taskNumber.'-'.$coefficientNumber, $functionName);
            $noiseLevel = 0.1;
            $rands = $this->generateFunctionData($functionName, $a, $count, $rangeMax, $rangeMin, $noiseLevel, $roundDigits);
            $data[$this->variant][$taskNumber] = $rands;
            file_put_contents($cachePath, json_encode($data));
        }
        else {
            $rands = $savedData;
        }

        return $rands;
    }

    public function getActiveData() {
        $seed = $this->getSeed($this->variant);
        $num = intval($this->numberInRange($seed, 0, count($this->activeDataCollection)-1));
        return $this->activeDataCollection[$num];
    }
}