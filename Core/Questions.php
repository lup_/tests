<?php
namespace Core;

class Questions
{
    private $basePath = "../Questions";
    private $courseCode;

    public function __construct($courseCode) {
        $this->courseCode = $courseCode;
    }

    public function getList($labCode) {
        $questionsConfig = "{$this->basePath}/{$this->courseCode}/{$labCode}.php";
        if (file_exists($questionsConfig)) {
            $questions = require($questionsConfig);
        }
        else {
            $questions = false;
        }
        return $questions;
    }

    private function getLabCodeFromFilename($filename) {
        return str_replace('.php', '', basename($filename));
    }

    public function getTotalList() {
        $totalQuestionList = [];
        foreach ( glob("{$this->basePath}/{$this->courseCode}/*.php") as $filename ) {
            $questions = require($filename);
            $labCode = $this->getLabCodeFromFilename($filename);

            foreach ($questions as $questionCode => $question) {
                $newCode = "{$labCode}.{$questionCode}";
                $totalQuestionList[ $newCode ] = $question;
            }
        }

        return $totalQuestionList;
    }

    private function getRandomKey(array $array, array $exclusions = []) {
        $keys = array_keys($array);
        $randomInt = round(rand(0, count($keys)-1));
        $randomKey = $keys[$randomInt];

        return in_array($randomKey, $exclusions)
            ? $this->getRandomKey($array, $exclusions)
            : $randomKey;
    }

    public function getRandomList($count = 40) {
        $allQuestions = $this->getTotalList();
        $randomQuestions = [];
        for ($num = 0; $num < $count; $num++) {
            $previousKeys = array_keys($randomQuestions);
            $key = $this->getRandomKey($allQuestions, $previousKeys);
            $randomQuestions[$key] = $allQuestions[$key];
        }
        return $randomQuestions;
    }

    public function checkAnswers($questions, $answers) {
        $result = [
            "correct" => 0,
            "wrong" => 0,
            "total" => count($questions),
        ];

        foreach ($questions as $index => $question) {
            $answer = $answers[$index];
            if ($answer == $question['correct']) {
                $result['correct']++;
            }
            else {
                $result['wrong']++;
            }
        }

        return $result;
    }
}