#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";stat_"$3"();quit();"
cd ../Matlab/Cmath/03-stat
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1
