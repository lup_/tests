#!/bin/bash
# $1 - vars
# $2 - variant
echo $1
echo $2
matlabCmd=$1";start();quit();"
cd ../Matlab/Cmath/05-integrate
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCmd" 2>&1

mkdir -p ../../../public_html/img/ans/Cmath/05-integrate/$2
mv -f *.png ../../../public_html/img/ans/Cmath/05-integrate/$2/