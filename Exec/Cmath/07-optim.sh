#!/bin/bash
# $1 - vars
# $2 - variant
echo $1
echo $2
matlabCmd=$1";start();quit();"
cd ../Matlab/Cmath/07-optim
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCmd" 2>&1
