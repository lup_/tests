#!/bin/bash
# $1 - vars
# $2 - variant
echo $1
echo $2
matlabCmd=$1";start();quit();"
cd ../Matlab/Cmath/08-ode
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCmd" 2>&1

mkdir -p ../../../public_html/img/ans/Cmath/08-ode/$2
mv -f *.png ../../../public_html/img/ans/Cmath/08-ode/$2/