#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";hydraulics_static_"$3"();quit();"
cd ../Matlab/Cmod/01-hstatic
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1
