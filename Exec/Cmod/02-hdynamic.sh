#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";hydraulics_dynamic_"$3"();quit();"
cd ../Matlab/Cmod/02-hdynamic
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1

mkdir -p ../../../public_html/img/ans/Cmod/02-hdynamic/$2
mv -f *.png ../../../public_html/img/ans/Cmod/02-hdynamic/$2/
