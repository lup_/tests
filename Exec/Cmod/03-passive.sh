#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";start_03();quit();"
cd ../Matlab/Cmod/03-passive
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1

mkdir -p ../../../public_html/img/ans/Cmod/03-passive/$2
mv -f *.png ../../../public_html/img/ans/Cmod/03-passive/$2/
