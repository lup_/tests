#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";react_"$3"();quit();"
cd ../Matlab/Cmod/05-react
pwd
echo $matlabCommand
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1

mkdir -p ../../../public_html/img/ans/Cmod/05-react/$2
mv -f *.png ../../../public_html/img/ans/Cmod/05-react/$2/
