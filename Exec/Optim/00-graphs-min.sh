#!/bin/bash
# $1 - vars
# $2 - variant
echo $1
echo $2
matlabCmd=$1";start();quit();"
cd ../Matlab/Optim/00-graphs-min
ls -la
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCmd" 2>&1

mkdir -p ../../../public_html/img/ans/Optim/00-graphs-min/$2
mv -f *.png ../../../public_html/img/ans/Optim/00-graphs-min/$2/