#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";temp_02_"$3"();quit();"
cd ../Matlab/Optim/01-02-reactor
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1

mkdir -p ../../../public_html/img/ans/Optim/02-reactor-temp/$2
mv -f *.png ../../../public_html/img/ans/Optim/02-reactor-temp/$2/
