#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";nletime_"$3"();quit();"
cd ../Matlab/Optim/04-reactor-time-nle
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1

mkdir -p ../../../public_html/img/ans/Optim/04-reactor-time-nle/$2
mv -f *.png ../../../public_html/img/ans/Optim/04-reactor-time-nle/$2/
