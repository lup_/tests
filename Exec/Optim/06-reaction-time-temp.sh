#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";main_"$3"();quit();"
cd ../Matlab/Optim/06-reaction-time-temp
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1

mkdir -p ../../../public_html/img/ans/Optim/06-reaction-time-temp/$2
mv -f *.png ../../../public_html/img/ans/Optim/06-reaction-time-temp/$2/
