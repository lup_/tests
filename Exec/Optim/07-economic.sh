#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";main_"$3"();quit();"
cd ../Matlab/Optim/07-economic
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1

mkdir -p ../../../public_html/img/ans/Optim/07-economic/$2
mv -f *.png ../../../public_html/img/ans/Optim/07-economic/$2/
