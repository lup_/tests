#!/bin/bash
# $1 - vars
# $2 - variant
# $3 - task
matlabCommand=$1";main;quit();"
cd Matlab/Optim/09-heat-flow-flow-cocurrent
sudo -u lup matlab -nodisplay -logfile debug.txt -r "$matlabCommand" 2>&1

