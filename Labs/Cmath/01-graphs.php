<?php
return [
    "name" => "Построение графиков",
    "type" => "multiple",
    "exec" => "/var/www/tests/Exec/Cmath/01-graphs.sh \":vars\" \":variant\"",
    "maxValue" => 7.5,
    "tasks" => [
        "2" => [
            "text" => "Построить график функции `f(x) = root(3)( (:A+:Bx)(:Cx^2+:Dx-:E) )`",
            "vars" => ["int(1-10)" => ["A", "B", "C", "D", "E"]],
            "answer" => ["type" => "graph"],
        ],
        "3" => [
            "text" => "Построить график функции `f(x) = frac(:Ax-:B)(sqrt(:Cx^2-:D))`",
            "vars" => ["int(1-10)" => ["A", "B", "C", "D"]],
            "answer" => ["type" => "graph"],
        ],
        "4" => [
            "text" => "Построить график функции `f(x) = frac(:Ax^3+:Bx^2-:Cx-:D)(:Ex^2-:F)`",
            "vars" => ["int(1-10)" => ["A", "B", "C", "D", "E", "F"]],
            "answer" => ["type" => "graph"],
        ],
        "5" => [
            "text" => "Построить графики функций на одной графической области: <br>`f(x) = :Ae^x`, <br>`g(x) = :B-:Cx^2`",
            "vars" => ["int(1-10)" => ["A", "B", "C"]],
            "answer" => ["type" => "graph"],
        ],
        "6" => [
            "text" => "Построить на плоскости кривую, заданную в параметрическом виде (`a=:A`): <br>`x(t) = frac(:Bat)(1+t^3)`, <br>`y(t) = frac(:Cat^2)(1+t^3)`",
            "vars" => ["float(1-10,1)" => ["A"], "int(1-10)" => ["B", "C"]],
            "answer" => ["type" => "graph"],
        ],
        "7" => [
            "text" => "Построить на плоскости кривую, заданную в параметрическом виде: <br>`x(t) = :A:F1^2(t)+:B:F2(t)`, <br>`y(t) = :C:F3(t):F4(t)`",
            "vars" => ["str(sin|cos)" => ["F1", "F2", "F3", "F4"], "int(1-10)" => ["A", "B", "C"]],
            "answer" => ["type" => "graph"],
        ],
        "8" => [
            "text" => "Построить кривые в полярных координатах: <br>`rho(varphi) = -:A:F1(:Bvarphi)`, <br>`rho(varphi) = :C:F2^2(frac(varphi)(:D))`, <br>`rho(varphi) = :Esqrt(:G:F3(varphi))`",
            "vars" => ["str(tg|ctg)"=> ["F1"], "str(sin|cos)" => ["F2", "F3"], "int(1-10)" => ["A", "B", "C", "D", "E", "G"]],
            "answer" => ["type" => "graph", "count" => 3],
        ],
        "9" => [
            "text" => "Построить кривые в полярных координатах: <br>`rho(varphi) = -:A:F1(:Bvarphi)`, <br>`rho(varphi) = 
:C:F2^2(frac(varphi)(:D))`, <br>`rho(varphi) = :Esqrt(:G:F3(varphi))`",
            "vars" => ["str(tg|ctg)"=> ["F1"], "str(sin|cos)" => ["F2", "F3"], "int(1-10)" => ["A", "B", "C", "D", "E", "G"]],
            "answer" => ["type" => "graph", "count" => 3],
        ],
        "11" => [
            "text" => "Построить поверхности различного типа: <br> `z(x, y)=:Ax^2−:By^2`, <br>`z(x, y)=:Cx^2−:Dy^2:F1^2(y)`, <br>`z(x, 
y)=:Exe^(:Fx)−:Gy`, <br>`z(x, y)=:F2(frac(x)(y)):F3(frac(y)(x))`, <br>`z(x, y)=ln(:Hx^2−:Iy^2)`, <br>`z(x, y)=sqrt(:Jy)(x^2+:K)`",
            "vars" => ["str(sin|cos)" => ["F1", "F2", "F3"], "int(1-10)" => ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"]],
            "answer" => ["type" => "graph", "count" => 6],
        ]
    ]
];