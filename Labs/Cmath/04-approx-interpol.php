<?php
return [
    "name" => "Аппроксимация и интерполяция",
    "type" => "multiple",
    "exec" => "/var/www/tests/Exec/Cmath/04-approx-interpol.sh \":vars\" \":variant\" \":task\"",
    "maxValue" => 7.5,
    "tasks" => [
        "1" => [
            "text" => "Определить интерполяционный многочлен Лагранжа L(x) по четырём узловым точкам:<br>
`bar x = (:INT_X)`<br>
`bar y = (:INT_Y)`",
            "vars" => ["func(xexp, 0-5, 4, 4)" => ["INT"]],
            "answer" => ["type" => "matlab"],
        ],

        "2" => [
            "text" => "Определить параметры зависимости вида `y=a_0 + a_1 x`, используя метод наименьших квадратов, по следующим экспериментальным данным:<br>
`bar x = (:APPROX_X)`<br>
`bar y = (:APPROX_Y)`",
            "vars" => ["func(lin, 0-5, 2, 6)" => ["APPROX"]],
            "answer" => ["type" => "matlab"],
        ],
    ]
];