<?php
return [
    "name" => "Численное интегрирование",
    "type" => "multiple",
    "exec" => "/var/www/tests/Exec/Cmath/05-integrate.sh \":vars\" \":variant\"",
    "maxValue" => 7.5,
    "tasks" => [
        "36" => [
            "text" => "Вычислить с шагом h=0.1 или 0.05 или 0.01<br>`int_(:FR)^(:TO) frac(1)(sqrt(:A+:Bx^3))dx`",
            "vars" => ["int(1-5)" => ["A", "B"], "float(1-2,1)" => ["FR"], "float(2-3,1)" => ["TO"]],
            "answer" => ["type" => "matlab"],
        ],
        "37" => [
            "text" => "Вычислить разбив на количество отрезков n=10 или 100<br>`int_(:FR)^(:TO) frac(1)(:A+sin(:Bx)+:Cx)dx`",
            "vars" => ["int(1-10)" => ["A", "B", "C"], "float(0-0.4,1)" => ["FR"], "float(0.5-1,1)" => ["TO"]],
            "answer" => ["type" => "matlab"],
        ],
        "38" => [
            "text" => "Интегрировать с шагом 0.001<br>`int_(:FR)^(:TO) sqrt(:Ax) :F1(:Bx^2) dx`",
            "vars" => ["int(1-10)" => ["A", "B", "C"], "float(0-0.4,1)" => ["FR"], "float(0.5-1,1)" => ["TO"], "str(sin|cos)" => ["F1"]],
            "answer" => ["type" => "matlab"],
        ],
        "39" => [
            "text" => "Вычислить площадь фигуры, ограниченной линиями `y^2=:Ax+:B`, и `x-y-:C=0`. Построить график",
            "vars" => ["int(1-10)" => ["A", "B", "C"]],
            "answer" => ["type" => "matlab_graph"],
        ],
        "40" => [
            "text" => "Вычислить<br>`int_(:FR)^(:TO) frac( :F1(:Ax) )( :Bx^2 - :C ) dx`",
            "vars" => ["int(1-10)" => ["A", "B", "C"], "float(0-1,1)" => ["FR"], "float(1-2,1)" => ["TO"], "str(sin|cos)" => ["F1"]],
            "answer" => ["type" => "matlab"],
        ],
        "41" => [
            "text" => "Вычислить с точностью 0.0001 или 0.0000001<br>`int_(:FR)^(:TO) frac( sqrt(:Ax+:B) )( :C:F1^2(x) ) dx`",
            "vars" => ["int(1-10)" => ["A", "B", "C"], "float(0-0.4,1)" => ["FR"], "float(0.5-1,1)" => ["TO"], "str(sin|cos)" => ["F1"]],
            "answer" => ["type" => "matlab"],
        ],
        "42" => [
            "text" => "Вычислить<br>`int_(:FR)^(:TO) sqrt(:Ax^2+:B) dx`",
            "vars" => ["float(0.1-1,1)"=>["A"], "int(1-10)" => ["B"], "float(0-1,1)" => ["FR"], "float(1-2,1)" => ["TO"]],
            "answer" => ["type" => "matlab"],
        ],
        "43" => [
            "text" => "<br>
A) Найти площадь фигуры ограниченной линиями `y^2+:Ax=:B`, и `y^2-:Cx=:D`.<br>
Б) Найти площадь фигуры, ограниченной линиями `y=:Ex^2` и `y=x^3/:F`",
            "vars" => ["int(2-10)" => ["E", "F"],  "int(1-20)" => ["A", "B"], "int(20-50)" => ["C", "D"]],
            "answer" => ["type" => "matlab_graph", "count" => 2],
        ],
        "44" => [
            "text" => "Вычислить<br>`int_(:FR)^(:TO) frac( e^x )( :A + :B:F1(:Cx) ) dx`",
            "vars" => ["int(1-10)" => ["A", "B", "C"], "float(0-0.4,1)" => ["FR"], "float(0.5-1,1)" => ["TO"], "str(sin|cos)" => ["F1"]],
            "answer" => ["type" => "matlab"],
        ],

    ]
];