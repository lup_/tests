<?php
return [
    "name" => "Построение графиков",
    "type" => "multiple",
    "exec" => "/var/www/tests/Exec/Inform/01-2d-graphs.sh \":vars\" \":variant\"",
    "maxValue" => 0,
    "tasks" => [
        "1" => [
            "text" => "Построить график функции `f(x) = -:Ax^3 - :Bx^2 + :Cx + :D`",
            "vars" => [
                "float(0.1-0.2,2)" => ["A", "B"],
                "float(6-10,2)" => ["C"],
                "float(1-20,2)" => ["D"],
            ],
            "answer" => ["type" => "graph"],
        ],
        "2" => [
            "text" => "Построить график функции `f(x) = -:Ax^5 + :Bx^4 + :Cx^3 - :Dx^2 - :Ex + :F`",
            "vars" => [
                "float(0.001-0.004,5)" => ["A"],
                "float(0-0.01,3)" => ["B"],
                "float(0.2-0.5,2)" => ["C"],
                "float(1-1.2,2)" => ["D"],
                "float(1-10,2)" => ["E","F"],
            ],
            "answer" => ["type" => "graph"],
        ],
        "3" => [
            "text" => "Построить график функции `f(x) = :A*frac(sin(:Bx))(x)`",
            "vars" => [
                "int(10-25)" => ["A"],
                "int(1-3)" => ["B"],
            ],
            "answer" => ["type" => "graph"],
        ],
    ]
];