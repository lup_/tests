<?php
return [
    "name" => "Построение поверхностей",
    "type" => "multiple",
    "exec" => "/var/www/tests/Exec/Inform/02-3d-graphs.sh \":vars\" \":variant\"",
    "maxValue" => 0,
    "tasks" => [
        "1" => [
            "text" => "Построить поверхность `z(x, y)=:Ax^2−:By^2` в интервале `x in [-10;10]`, `y in [-10;10]`",
            "vars" => ["int(1-10)" => ["A", "B"]],
            "answer" => ["type" => "graph"],
        ],
        "2" => [
            "text" => "Построить поверхность `z(x, y)=sqrt(:Ay)(x^2+:B)` в интервале `x in [-10;10]`, `y in [0;20]`",
            "vars" => ["int(1-10)" => ["A", "B"]],
            "answer" => ["type" => "graph"],
        ],
        "3" => [
            "text" => "Построить поверхность `z(x, y)=:C*e^(-(x -:A)^2 -(y - :B)^2)` в интервале `x in [-5;5]`, `y in [-5;5]`",
            "vars" => ["int(1-5)" => ["A", "B","C"]],
            "answer" => ["type" => "graph"],
        ],
        "4" => [
            "text" => "Построить поверхность `z(x, y)=frac(:Ax^2+:By^2)(:C)` в интервале `x in [-5;5]`, `y in [-5;5]`",
            "vars" => ["int(1-5)" => ["A", "B", "C"]],
            "answer" => ["type" => "graph"],
        ],
    ]
];