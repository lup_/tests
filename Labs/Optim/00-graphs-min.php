<?php
return [
    "name" => "Графики и одномерная оптимизация",
    "type" => "multiple",
    "exec" => "/var/www/tests/Exec/Optim/00-graphs-min.sh \":vars\" \":variant\"",
    "maxValue" => 6,
    "tasks" => [
        "1" => [
            "text" => "Построить график функции `f(x) = root(3)( (:A+:Bx)(:Cx^2+:Dx-:E) )`",
            "vars" => ["int(1-10)" => ["A", "B", "C", "D", "E"]],
            "answer" => ["type" => "graph"],
        ],
        "2" => [
            "text" => "Построить график функции `f(x) = frac(:Ax-:B)(sqrt(:Cx^2-:D))`",
            "vars" => ["int(1-10)" => ["A", "B", "C", "D"]],
            "answer" => ["type" => "graph"],
        ],
        "3" => [
            "text" => "Построить график функции `f(x) = frac(:Ax^3+:Bx^2-:Cx-:D)(:Ex^2-:F)`",
            "vars" => ["int(1-10)" => ["A", "B", "C", "D", "E", "F"]],
            "answer" => ["type" => "graph"],
        ],
        "4" => [
            "text" => "Построить график поверхности: `z(x, y)=:Ax^2−:By^2`",
            "vars" => ["int(1-10)" => ["A", "B"]],
            "answer" => ["type" => "graph"],
        ],
        "5" => [
            "text" => "Найти локальные минимумы функции `f(x)=:Ax^4-:Bx^2+:C`",
            "vars" => ["int(1-20)" => ["A", "B", "C"]],
            "answer" => ["type" => "matlab"],
        ],
    ],
];