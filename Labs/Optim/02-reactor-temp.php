<?php
return [
    "name" => "Определение оптимальной температуры в непрерывном реакторе с мешалкой",
    "type" => "single",
    "exec" => "/var/www/tests/Exec/Optim/02-reactor-temp.sh \":vars\" \":variant\" \":task\"",
    "maxValue" => 6,
    "tasks" => [
        "1" => [
            "text" => "<p>Рассчитать оптимальную температуру проведения обратимой двухкомпонентной реакции в изотермическом реакторе с мешалкой, используя в качестве критерия оптимальности выход целевого продукта P.</p>
    <p>`{:(,k_1,),(A,rarr,:KPP):}`</p>
    <p>`{:(,k_2,),(P,rarr,:KSS):}`</p>
    <p>`A_1 = :A1, \"мин\"^(-1); A_2 = :A2, \"мин\"^(-1)`</p>
    <p>`E_1 = :E1, \"кал/моль\"; E_2 = :E2, \"кал/моль\"`</p>
    <p>`tau = :TAU, \"мин\"`</p>
    <p>`x_A^0 = :XA, \"мольные доли\"`</p>
    ",
            "vars" => [
                "int(1-5)" => ["KP", "KS"],
                "int(110-190)" => ["A1"],
                "int(30-70)" => ["A2"],
                "int(3000-4500)" => ["E1"],
                "int(5500-6500)" => ["E2"],
                "int(10-20)" => ["TAU"],
                "float(0.01-1,2)" => ["XA"],
            ],
            "answer" => ["type" => "matlab_graph"],
        ],
        "2" => [
            "text" => "<p>Рассчитать оптимальную температуру проведения обратимой двухкомпонентной реакции в изотермическом реакторе с мешалкой, используя в качестве критерия оптимальности выход целевого продукта P.</p>
    <p>`{:(,k_1,),(A,rarr,:KPAP + :KCC):}`</p>
    <p>`{:(,k_2,),(B,rarr,:KPBP):}`</p>
    <p>`{:(,k_3,),(P,rarr,:KSS):}`</p>
    <p>`A_1 = :A1, \"мин\"^(-1); A_2 = :A2, \"мин\"^(-1); A_3 = :A3, \"мин\"^(-1)`</p>
    <p>`E_1 = :E1, \"кал/моль\"; E_2 = :E2, \"кал/моль\"; E_3 = :E3, \"кал/моль\"`</p>
    <p>`tau = :TAU, \"мин\"`</p>
    <p>`x_A^0 = :XA, \"мольные доли\"; x_B^0 = :XB, \"мольные доли\"`</p>
    ",
            "vars" => [
                "int(1-5)" => ["KPA", "KPB", "KS", "KC"],
                "int(50-90)" => ["A1", "A2"],
                "int(75-135)" => ["A3"],
                "int(2000-3000)" => ["E1"],
                "int(5500-6500)" => ["E2"],
                "int(6500-7500)" => ["E3"],
                "int(10-20)" => ["TAU"],
                "float(0.01-1,2)" => ["XA", "XB"],
            ],
            "answer" => ["type" => "matlab_graph"],
        ],
        "3" => [
            "text" => "<p>Рассчитать оптимальную температуру проведения обратимой двухкомпонентной реакции в изотермическом реакторе с мешалкой, используя в качестве критерия оптимальности выход целевого продукта P.</p>
    <p>`{:(,k_1,),(A,rarr,:KPAP + :KBAB):}`</p>
    <p>`{:(,k_2,),(P,rarr,:KSS):}`</p>
    <p>`{:(,k_3,),(P,rarr,:KBPB):}`</p>
    <p>`A_1 = :A1, \"мин\"^(-1); A_2 = :A2, \"мин\"^(-1); A_3 = :A3, \"мин\"^(-1)`</p>
    <p>`E_1 = :E1, \"кал/моль\"; E_2 = :E2, \"кал/моль\"; E_3 = :E3, \"кал/моль\"`</p>
    <p>`tau = :TAU, \"мин\"`</p>
    <p>`x_A^0 = :XA, \"мольные доли\"`</p>
    ",
            "vars" => [
                "int(1-5)" => ["KPA", "KBA", "KS", "KBP"],
                "int(50-90)" => ["A1"],
                "int(75-135)" => ["A2", "A3"],
                "int(1000-2000)" => ["E1"],
                "int(5000-6000)" => ["E2"],
                "int(6000-7000)" => ["E3"],
                "int(10-20)" => ["TAU"],
                "float(0.01-1,2)" => ["XA"],
            ],
            "answer" => ["type" => "matlab_graph"],
        ],
        "4" => [
            "text" => "<p>Рассчитать оптимальную температуру проведения обратимой двухкомпонентной реакции в изотермическом реакторе с мешалкой, используя в качестве критерия оптимальности выход целевого продукта P.</p>
    <p>`{:(,k_1,),(A,harr,P):}`</p>
    <p>`{:(,k_2,),(P,rarr,:KSS):}`</p>
    <p>`A_1 = :A1, \"мин\"^(-1); A_2 = :A2, \"мин\"^(-1); A_3 = :A3, \"мин\"^(-1)`</p>
    <p>`E_1 = :E1, \"кал/моль\"; E_2 = :E2, \"кал/моль\"; E_3 = :E3, \"кал/моль\"`</p>
    <p>`tau = :TAU, \"мин\"`</p>
    <p>`x_A^0 = :XA, \"мольные доли\"`</p>
    ",
            "vars" => [
                "int(1-5)" => ["KS"],
                "int(110-130)" => ["A1"],
                "int(50-70)" => ["A2"],
                "int(70-90)" => ["A3"],
                "int(4000-5000)" => ["E1", "E2"],
                "int(5000-6000)" => ["E3"],
                "int(10-20)" => ["TAU"],
                "float(0.01-1,2)" => ["XA"],
            ],
            "answer" => ["type" => "matlab_graph"],
        ],
        "5" => [
            "text" => "<p>Рассчитать оптимальную температуру проведения обратимой двухкомпонентной реакции в изотермическом реакторе с мешалкой, используя в качестве критерия оптимальности выход целевого продукта P.</p>
    <p>`{:(,k_1,),(A,harr,P):}`</p>
    <p>`{:(,k_2,),(P,harr,S):}`</p>
    <p>`A_1 = :A1, \"мин\"^(-1); A_2 = :A2, \"мин\"^(-1); A_3 = :A3, \"мин\"^(-1); A_4 = :A4, \"мин\"^(-1)`</p>
    <p>`E_1 = :E1, \"кал/моль\"; E_2 = :E2, \"кал/моль\"; E_3 = :E3, \"кал/моль\"; E_4 = :E4, \"кал/моль\"`</p>
    <p>`tau = :TAU, \"мин\"`</p>
    <p>`x_A^0 = :XA, \"мольные доли\"`</p>
    ",
            "vars" => [
                "int(120-140)" => ["A1"],
                "int(50-70)" => ["A2", "A4"],
                "int(70-90)" => ["A3"],
                "int(5000-6000)" => ["E1", "E2"],
                "int(6000-7000)" => ["E3", "E4"],
                "int(10-20)" => ["TAU"],
                "float(0.01-1,2)" => ["XA"],
            ],
            "answer" => ["type" => "matlab_graph"],
        ],
    ],
];