%2
x = -10:0.1:10;
y = ( (A_2+B_2.*x).*(C_2.*x.^2+D_2.*x-E_2) ).^(1/3);
ph = plot(x, y);
saveas(ph, '2.png');

%3
x = -10:0.1:10;
y = (A_3.*x-B_3)./(sqrt(C_3.*x.^2-D_3));
ph = plot(x, y);
saveas(ph, '3.png');

%4
x = -10:0.1:10;
y = (A_4.*x.^3+B_4.*x.^2-C_4.*x-D_4)./(E_4.*x.^2-F_4);
ph = plot(x, y);
saveas(ph, '4.png');

%5
x = -2:0.1:2;
f_5 = A_5.*exp(x);
g_5 = B_5-C_5.*x.^2;
ph = figure;
plot(x, f_5, x, g_5);
saveas(ph, '5.png');

%6
t = -10:0.1:10;
a = A_6;
x = (B_6.*a.*t)./(1+t.^3);
y = (C_6.*a.*t.^2)./(1+t.^3);
ph = plot(x, y);
saveas(ph, '6.png');

%7
t = -3*pi:0.1:3*pi;
xs = strcat(int2str(A_7), '.*(', F1_7, '(t)).^2+', int2str(B_7), '.*', F2_7, '(t)');
ys = strcat(int2str(C_7), '.*', F3_7, '(t).*', F4_7, '(t)');
xt = inline( xs );
yt = inline( ys );
x = xt(t);
y = yt(t);
ph = plot(x, y);
saveas(ph, '7.png');

%8
if ( strcmp(F1_8,'tg') )
    F1_8 = 'tan';
else
    F1_8 = 'cot';
end;
phi = 0:0.1:2*pi;
rhos = strcat( '-', int2str(A_8), '.*', F1_8, '(', int2str(B_8), '.*x)' );
rhof = inline( rhos );
ph = figure;
polarplot(phi, rhof(phi));
saveas(ph, '8_1.png');

rhos = strcat( int2str(C_8), '.*(', F2_8, '(x./', int2str(D_8), ')).^2' );
rhof = inline( rhos );
ph = figure;
polarplot(phi, rhof(phi));
saveas(ph, '8_2.png');

rhos = strcat( int2str(E_8), '.*sqrt(', int2str(G_8), '.*', F3_8, '(x))' );
rhof = inline( rhos );
ph = figure;
polarplot(phi, rhof(phi));
saveas(ph, '8_3.png');

%9
if ( strcmp(F1_9,'tg') )
    F1_9 = 'tan';
else
    F1_9 = 'cot';
end;
phi = 0:0.1:2*pi;
rhos = strcat( '-', int2str(A_9), '.*', F1_9, '(', int2str(B_9), '.*x)' );
rhof = inline( rhos );
ph = figure;
polarplot(phi, rhof(phi));
saveas(ph, '9_1.png');

rhos = strcat( int2str(C_9), '.*(', F2_9, '(x./', int2str(D_9), ')).^2' );
rhof = inline( rhos );
ph = figure;
polarplot(phi, rhof(phi));
saveas(ph, '9_2.png');

rhos = strcat( int2str(E_9), '.*sqrt(', int2str(G_9), '.*', F3_9, '(x))' );
rhof = inline( rhos );
ph = figure;
polarplot(phi, rhof(phi));
saveas(ph, '9_3.png');

%11
[x, y] = meshgrid(-10:0.5:10, -10:0.5:10);
z = A_11.*x.^2 - B_11.*y.^2;
ph = figure;
surf(x, y, z);
saveas(ph, '11_1.png');

[x, y] = meshgrid(-10:0.5:10, -10:0.5:10);
zs = strcat( int2str(C_11), '.*x.^2 - ', int2str(D_11), '.*y.^2.*(', F1_11,'(y)).^2' );
zf = inline( zs );
z = zf(x,y);
ph = figure;
surf(x, y, z);
saveas(ph, '11_2.png');

[x, y] = meshgrid(-1:0.05:1, -1:0.05:1);
z = E_11.*x.*exp(F_11.*x) - G_11.*y;
ph = figure;
surf(x, y, z);
saveas(ph, '11_3.png');

[x, y] = meshgrid(-10:0.5:10, -10:0.5:10);
zs = strcat( F2_11, '(x./y).*', F3_11, '(y./x)' );
zf = inline( zs );
z = zf(x,y);
ph = figure;
surf(x, y, z);
saveas(ph, '11_4.png');

[x, y] = meshgrid(9:0.05:10, 0:0.05:1);
z = log(H_11.*x.^2 - I_11.*y.^2);
ph = figure;
surf(x, y, z);
saveas(ph, '11_5.png');

[x, y] = meshgrid(-10:0.5:10, 0:0.5:20);
z = sqrt(J_11.*y).*(x.^2+K_11);
ph = figure;
surf(x, y, z);
saveas(ph, '11_6.png');