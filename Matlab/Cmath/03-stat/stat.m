function stat(x, t, u)

    needsRecount = true;

    while needsRecount
        n = numel(x);
        avgx = mean(x);
        stdev = std(x);

        disp('Среднее: ');
        disp(avgx);
        disp('Дисперсия: ');
        disp(stdev);

        [dxp, kx] = max( abs(x - mean(x)) );
        xp = x(kx);

        disp('Подозрение на выброс: ');
        disp(xp);

        ur = dxp/sqrt( stdev*(n-1)/n );
        fu = n - 2;
        ut = u(fu);
        isError = ur > ut;

        disp('Ur: ');
        disp(ur);
        disp('Ut: ');
        disp(ut);

        needsRecount = false;
        if isError
            needsRecount = true;
            disp('Ur > Ut - выброс, пересчитываем');
            x(kx) = [];
        end;
    end;

    disp('Ur < Ut - выброса нет');
    ft = n - 1;
    tt = t(ft);

    eps = tt * sqrt( stdev/n );
    disp('x');
    disp(avgx);
    disp('eps: +-');
    disp(eps);
end