warning('off', 'all');

%36
disp('=== 36');
for step=[0.1 0.05 0.01]
    x = FR_36:step:TO_36;
    y = 1./(A_36+B_36*x.^3);
    int = trapz(x,y);
    disp(sprintf('Шаг %G: %G', step, int));
end;
disp('===');

%37
disp('=== 37');
for n=[10 100]
    step = (TO_37 - FR_37)/n;
    x = FR_37:step:TO_37;
    y = 1./(A_37+sin(B_37*x)+C_37*x);
    int = trapz(x,y);
    disp(sprintf('Количество шагов %G: %G', n, int));
end
disp('===');

%38
disp('=== 38');
ys = strcat('sqrt(', int2str(A_38),'*x) .* ', F1_38, '(', int2str(B_38), '*x.^2)');
yf = inline(ys);
x = FR_38:0.001:TO_38;
y = yf(x);
trapz(x,y)
disp('===');

%39
disp('=== 39');
Z_1 = -B_39/A_39;
Z_2 = C_39;

a = 1;
b = -(2*C_39 + A_39);
c = (C_39^2 - B_39);

x1 = (-b + sqrt(b^2 - 4*a*c))/(2*a);
x2 = (-b - sqrt(b^2 - 4*a*c))/(2*a);

Z_3 = x1;

x1 = Z_1:0.01:Z_3;
y1 = sqrt(A_39*x1 + B_39);
i1 = trapz(x1,y1);

x2 = Z_2:0.01:Z_3;
y2 = x2 - C_39;
i2 = trapz(x2, y2);

S = i1 - i2;

disp(sprintf('int1 [%G;%G] = %G', Z_1, Z_3, i1));
disp(sprintf('int2 [%G;%G] = %G', Z_2, Z_3, i2));
disp(sprintf('S = int1 - int2 = %G', S));

x = (Z_1-1):0.01:(Z_3+1);
y1 = sqrt(A_39*x + B_39);
y2 = x - C_39;
ph = figure;
plot(x, y1, x, y2, x, zeros(1, numel(x)), 'k');
grid
saveas(ph, '39.png');

disp('===');

%40
disp('=== 40');
ys = strcat( F1_40, '(', int2str(A_40), '*x)./(', int2str(B_40), '*x.^2 - ', int2str(C_40), ')' );
yf = inline(ys);
x = FR_40:0.001:TO_40;
y = yf(x);
trapz(x,y)

disp('===');

%41
disp('=== 41');

ys = strcat( 'sqrt(', int2str(A_41), '*x + ', int2str(B_41), ')./(', int2str(C_41), '*', F1_41, '(x).^2)' );
yf = inline(ys);

for tol=[0.0001 0.0000001]
    int = integral(@(x) yf(x), FR_41, TO_41, 'RelTol', 0, 'AbsTol',tol);
    disp(sprintf('Точность %G: %0.8g', tol, int));
end;
disp('===');

%42
disp('=== 42');
integral(@(x) sqrt(A_42*x.^2+B_42), FR_42, TO_42)
disp('===');

%43
disp('=== 43');

disp('А:');
y1 = @(x) sqrt(D_43+C_43*x);
y2 = @(x) sqrt(B_43-A_43*x);

Z_1 = B_43/A_43;
Z_2 = -D_43/C_43;
Z_3 = (B_43-D_43)/(C_43+A_43);

i1 = integral(y1, Z_2, Z_3);
i2 = integral(y2, Z_3, Z_1);
S = i1 + i2;
disp(sprintf('int1 [%G;%G] = %G', Z_2, Z_3, i1));
disp(sprintf('int2 [%G;%G] = %G', Z_3, Z_1, i2));
disp(sprintf('S = int1 + int2 = %G', S));

x = (Z_2-0.5):0.01:(Z_1+0.5);
ph = figure;
plot(x, y1(x), x, y2(x));
grid
saveas(ph, '43_1.png');

disp(' ');
disp('Б:');
y1 = @(x) E_43*x.^2;
y2 = @(x) x.^3/F_43;

Z_1 = 0;
Z_2 = E_43*F_43;

i1 = integral(y1, Z_1, Z_2);
i2 = integral(y2, Z_1, Z_2);
S = i1 - i2;
disp(sprintf('int1 [%G;%G] = %G', Z_1, Z_2, i1));
disp(sprintf('int2 [%G;%G] = %G', Z_1, Z_2, i2));
disp(sprintf('S = int1 - int2 = %G', S));

x = (Z_1-1):0.1:(Z_2+1);
ph = figure;
plot(x, y1(x), x, y2(x));
grid
saveas(ph, '43_2.png');

disp('===');

%44
disp('=== 44');
ys = strcat( 'exp(x)./(', int2str(A_44), ' + ', int2str(B_44), '*', F1_44, '(', int2str(C_44), '*x))' );
yf = inline(ys);
integral(@(x) yf(x), FR_44, TO_44)
disp('===');
