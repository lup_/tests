%26
disp('=== 26');
p1 = [A_26 0 -B_26 0 C_26];
p2 = [D_26 0 E_26 -F_26];
pm = conv(p1, p2);
disp('Коэффициенты полинома-произведения:');
disp(pm);
disp('===');

%27
disp('=== 27');
p1 = [A_27 -B_27 C_27 0 -D_27 E_27 -F_27];
p2 = [G_27 0 H_27 -I_27];
[q, r] = deconv(p1,p2);

disp('Коэффициенты полинома, частного от деления:');
disp(q);
disp('Коэффициенты полинома, остатка от деления:');
disp(r);
disp('===');

%28
disp('=== 28');
p = [A_28 -B_28 -C_28 -D_28];
a = roots(p);
disp('Корни полинома:');
disp(a);

lb = 0.9*min(a);
ub = 1.1*max(a);
x = real(lb):0.1:real(ub);
y = polyval(p, x);
disp('На графике отражена только действительная часть функции');
ph = figure;
plot(x, real(y));
grid;
saveas(ph, '28.png');
disp('===');

%29
disp('=== 29');
p = [A_29 -B_29 -C_29 0 D_29];
a = roots(p);
disp('Корни полинома:');
disp(a);

lb = 0.9*min(a);
ub = 1.1*max(a);
x = real(lb):0.1:real(ub);
y = polyval(p, x);
disp('На графике отражена только действительная часть функции');
ph = figure;
plot(x, real(y));
grid;
saveas(ph, '29.png');
disp('===');

%30
disp('=== 30');
f = @(x) A_30.^x+B_30.*x-C_30;
r = fzero(f, 0);
disp('Корень уравнения:');
disp(r);

x = -2:0.1:2;
ph = figure;
plot(x, f(x));
grid;
saveas(ph, '30.png');
disp('===');

%31
disp('=== 31');
f = @(x) A_31.*x - B_31;
g = @(x) C_31.*log(D_31.*x);
z = @(x) f(x) - g(x);

r = fzero(z, 10);
disp('Корень уравнения:');
disp(r);

x = 0.1:0.1:6;
ph = figure;
plot(x, f(x), x, g(x));
grid;
saveas(ph, '31.png');
disp('===');

%32
disp('=== 32');
f = @(x) cot(A_32.*x);
g = @(x) x/B_32;
z = @(x) f(x) - g(x);

r = fzero(z, [0.01 pi/A_32]);
disp('Один из корней уравнения:');
disp(r);

x = -0.1:0.01:(pi/A_32+0.1);
ph = figure;
plot(x, f(x), x, g(x));
grid;
axis([min(x), max(x), -10 10]);
saveas(ph, '32.png');
disp('===');

%33
disp('=== 33');

f = @(x) [cos(A1_33.*x(1))+B1_33.*x(2)-C1_33 x(1).^2./A2_33-x(2).^2./B2_33-C2_33];
r1 = fsolve(f, [-4 2], optimoptions('fsolve','Display','off'));
r2 = fsolve(f, [4 2], optimoptions('fsolve','Display','off'));
disp('Решения системы уравнений:');
disp(r1);
disp(r2);

x = -10:0.01:10;
y1 = (cos(A1_33.*x)-C1_33)./-B1_33;
y2 = real( sqrt( B2_33.*(x.^2./A2_33-C2_33) ) );

ph = figure;
plot(x, y1, x, y2);
grid;
saveas(ph, '33.png');

disp('===');

%34
disp('=== 34');

f = @(x) [A1_34.*x(2)-x(1)^2./B1_34-C1_34  A2_34.*sin(x(1)-B2_34)+C2_34.*x(2)-D2_34];
r1 = fsolve(f, [-10 5], optimoptions('fsolve','Display','off'));
r2 = fsolve(f, [10 5], optimoptions('fsolve','Display','off'));
disp('Решения системы уравнений:');
disp(r1);
disp(r2);


x = -10:0.01:10;
y1=(x.^2./B1_34-C1_34)./A1_34;
y2 = (A2_34.*sin(x-B2_34)-D2_34)./-C2_34;

ph = figure;
plot(x, y1, x, y2);
grid;
saveas(ph, '34.png');

disp('===');

%35
disp('=== 35');
f = @(x) [A1_35.*x(1).^2+B1_35.*x(2).^2-C1_35  A2_35.*x(1).^2-B2_35.*x(2).^2-C2_35];
r1 = fsolve(f, [-10 0], optimoptions('fsolve','Display','off'));
r2 = fsolve(f, [10 0], optimoptions('fsolve','Display','off'));
disp('Решения системы уравнений:');
disp(r1);
disp(r2);

x = -5:0.01:5;
y1=sqrt( (A1_35.*x.^2-C1_35)./-B1_35 );
y2=sqrt( (A2_35.*x.^2-C2_35)./B2_35 );

ph = figure;
plot(x, real(y1), 'b-', x, real(y2), 'r-', x, imag(y1), 'b--', x, imag(y2), 'r--');
grid;
saveas(ph, '35.png');
disp('===');
