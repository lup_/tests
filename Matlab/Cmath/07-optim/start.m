warning('off', 'all');
nodisplay = optimset('Display', 'off');

%82
disp('=== 82');
f = @(x) A_82.*x.^4-B_82.*x.^2+C_82;
[x1, y1] = fminbnd(f, -10, 0);
[x2, y2] = fminbnd(f, 0, 10);
disp('Локальные минимумы функции:');
disp([x1 y1]);
disp([x2 y2]);
disp('===');

%83
disp('=== 83');
f = @(x) A_83.*x(1).^2-B_83.*x(1).*x(2)+C_83.*x(2).^2+D_83.*x(1)-E_83.*x(2)+F_83;
[x,f] = fminsearch(f, [0 0]);
disp('Минимум функции:');
disp(sprintf('x = %0.4f, y = %0.4f, f(x,y) = %0.4f', x(1), x(2), f));
disp('===');

%84
disp('=== 84');
w = [A_84 B_83];
A = [-1 -1; 1 1; 1 2; -2 2; 1 3];
b = [-2 4 3 -4 4];
[x, fw] = linprog(w, A, b, [], [], [], [], nodisplay);
disp('Минимум функции, удовлетворяющий ограничениям:');
disp(sprintf('a = %0.4f, b = %0.4f, w(a,b) = %0.4f', x(1), x(2), fw-C_83));
disp('===');

%85
disp('=== 85');
w = [A_85 B_85 C_85];
A = -[1 1 1; 2 1 -1];
b = -[1 -1];
Aeq = [1 -1 1];
beq = [0];
lb = [0 0 0];
ub = [1 1 1];
[x, fw] = linprog(w, A, b, Aeq, beq, lb, ub, nodisplay);
disp('Минимум функции, удовлетворяющий ограничениям:');
disp(sprintf('a = %0.4f, b = %0.4f, c = %0.4f, w(a,b,c) = %0.4f', x(1), x(2), x(3), fw));
disp('===');

%86
disp('=== 86');
w = [A_86 B_86];
A = -[1 1; 1 -1];
b = -[2 1];
lb = [0 0];
[x, fw] = linprog(w, A, b, [], [], lb, [], nodisplay);
disp('Минимум функции, удовлетворяющий ограничениям:');
disp(sprintf('a = %0.4f, b = %0.4f, w(a,b) = %0.4f', x(1), x(2), fw));
disp('===');

disp('===');

%87
disp('=== 87');
w = @(x) A_87.*x(1).^2+B_87.*x(2)^2 - C_87;
x0 = [0 0];
lb = [0 0];
nonlcon = @(x) deal([], [x(1).*x(2)-1]);

[x, fw] = fmincon(w, x0, [], [], [], [], lb, [], nonlcon, nodisplay);
disp('Минимум функции, удовлетворяющий ограничениям:');
disp(sprintf('a = %0.4f, b = %0.4f, w(a,b) = %0.4f', x(1), x(2), fw));

disp('===');
