%45
disp('=== 45');

odefun = @(t, x) [x(2); B_45/A_45*x(2) - C_45/A_45*x(1)];
tspan = [IS_45 IE_45];
x0 = [-1; 0.5];
[t, y] = ode45(odefun, tspan, x0);

ph = figure;
plot(t, y(:, 1));
saveas(ph, '45.png');

disp('===');

%46
disp('=== 46');

odefun = @(x, y) [B_46 * y * log(C_46*y) / cos(A_46 * x)];
tspan = [pi/D_46 2*pi];
x0 = [exp(1)];
[x, y] = ode45(odefun, tspan, x0);

ph = figure;
plot(x, y(:, 1));
disp(y(:,1));
saveas(ph, '46.png');

disp('===');

%47
disp('=== 47');

odefun = @(t, x) [ x(2); 1/A_47*(B_47*x(2) - C_47*x(1) - sin(D_47*t) - cos(E_47*t)) ];
tspan = [IS_47 IE_47];
x0 = [5; 0.25];
[t, y] = ode45(odefun, tspan, x0);

ph = figure;
plot(t, y(:, 1));
saveas(ph, '47.png');

disp('===');

%48
disp('=== 48');

odefun = @(t, y) [ cos(A_48*y(1)+B_48*y(2)); sin(C_48*y(1) - D_48*y(2)) ];
tspan = [IS_48 IE_48];
x0 = [XN_48 YN_48];
[t, y] = ode15s(odefun, tspan, x0);

ph = figure;
plot(t, y(:, 1), t, y(:, 2));
title('A, ode15s');
saveas(ph, '48_1.png');

[t, y] = ode23s(odefun, tspan, x0);

ph = figure;
plot(t, y(:, 1), t, y(:, 2));
title('Б, ode23s');
saveas(ph, '48_2.png');

disp('===');

%49
disp('=== 49');

odefun = @(t, y) [ A_49*y(1)^2/(B_49*y(2)); C_49*y(1)^2 - D_49*y(2) ];
tspan = [IS_49 IE_49];
x0 = [XN_49 YN_49];
[t, y] = ode45(odefun, tspan, x0);

ph = figure;
plot(t, y(:, 1), t, y(:, 2));
saveas(ph, '49.png');

disp('===');

%50
disp('=== 50');
disp('NA');
disp('===');

%52
disp('=== 52');

%odefun = @(x, y) [ y(1)^2 - A_52*y(1)*y(2); C_52*y(2)*(x + y(1))/(x^2 - D_52*y(2)*y(1)) ];
%tspan = [IS_52 IE_52];
%x0 = [YN_52 ZN_52];
%[t, y] = ode45(odefun, tspan, x0);

%ph = figure;
%plot(t, y(:, 1), t, y(:, 2));
%saveas(ph, '52.png');

disp('===');

%53
disp('=== 53');
disp('NA');
disp('===');
