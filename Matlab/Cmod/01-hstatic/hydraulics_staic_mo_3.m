function [fx, p, v, k] = hydraulics_staic_mo_3( x )
   [p, k, hg, pn, ro, g] = input_data_3();
   h = zeros(1,2);
   v = zeros(1,7);
   
   h(1) = x;
   p(9) = pn * hg(1) / (hg(1) - h(1));
   p(7) = p(9) + ro * g * h(1);
   v(1) = k(1) * sign(p(1) - p(7)) * sqrt(abs(p(1) - p(7)));
   v(2) = k(2) * sign(p(2) - p(7)) * sqrt(abs(p(2) - p(7)));
   v(3) = k(3) * sign(p(3) - p(7)) * sqrt(abs(p(3) - p(7)));
   v(4) = k(4) * sign(p(4) - p(7)) * sqrt(abs(p(4) - p(7)));
   v(5) = k(5) * sign(p(5) - p(7)) * sqrt(abs(p(5) - p(7)));
   v(7) = v(1) + v(2) + v(3) + v(4) + v(5);
   p(8) = p(7) - sign(v(7)) * (v(7) / k(7)) ^ 2;
   v(6) = k(6) * sign(p(8) - p(6)) * sqrt(abs(p(8) - p(6)));
   fx = (v(7) - v(6)) * ro;
end

