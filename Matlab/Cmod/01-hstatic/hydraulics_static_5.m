[p, k, hg, pn, ro, g] = input_data_5();
eps=1e-3;

h = zeros(1, 2);
h(1) = fzero(@hydraulics_staic_mo_5, [eps, hg(1)-eps]);
[fx, p, v] = hydraulics_staic_mo_5(h(1));

a = -ro * g; b = p(8) + ro * g * hg(2); c = (pn-p(8)) * hg(2);

h(2) = (-b - sqrt(b * b - 4 * a * c)) / (2 * a);
if (h(2) < 0) || (h(2) > hg(2))
   h(2) = (-b + sqrt(b * b - 4 * a * c)) / (2 * a);
end

p(10) = pn * hg(2) / (hg(2) - h(2));
vm = v*ro;

disp('h[1-2]=');
disp(h(1:2));

disp('p[7-10]=');
disp(p(7:10));

disp('v[1-7]=');
disp(vm(1:7));