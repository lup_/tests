function [p, k, hg, pn, ro, g] = input_data_1()
    global P1_1 P2_1 P3_1 P4_1 P5_1 P6_1;
    global KEXT_1 KINT_1;
    global HG1_1 HG2_1;

    pn = 1e5;
    ro = 1000;
    g = 9.81;
    S = 0.01;

    p = [P1_1*1e6, P2_1*1e6, P3_1*1e6, P4_1*1e5, P5_1*1e5, P6_1*1e5, 0, 0, 0, 0];
    k = [KEXT_1*S/sqrt(ro), KEXT_1*S/sqrt(ro), KEXT_1*S/sqrt(ro), KEXT_1*S/sqrt(ro), KEXT_1*S/sqrt(ro), KEXT_1*S/sqrt(ro), KINT_1*S/sqrt(ro)];
    hg = [HG1_1, HG2_1];
end