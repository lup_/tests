function [p, k, hg, pn, ro, g] = input_data_2()
    global P1_2 P2_2 P3_2 P4_2 P5_2 P6_2;
    global KEXT_2 KINT_2;
    global HG1_2 HG2_2;

    pn = 1e5;
    ro = 1000;
    g = 9.81;
    S = 0.01;

    p = [P1_2*1e6, P2_2*1e6, P3_2*1e6, P4_2*1e6, P5_2*1e5, P6_2*1e5, 0, 0, 0, 0];
    k = [KEXT_2*S/sqrt(ro), KEXT_2*S/sqrt(ro), KEXT_2*S/sqrt(ro), KEXT_2*S/sqrt(ro), KEXT_2*S/sqrt(ro), KEXT_2*S/sqrt(ro), KINT_2*S/sqrt(ro)];
    hg = [HG1_2, HG2_2];
end