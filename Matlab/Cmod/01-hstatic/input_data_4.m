function [p, k, hg, pn, ro, g] = input_data_4()
    global P1_4 P2_4 P3_4 P4_4 P5_4 P6_4;
    global KEXT_4 KINT_4;
    global HG1_4 HG2_4;

    pn = 1e5;
    ro = 1000;
    g = 9.81;
    S = 0.01;

    p = [P1_4*1e6, P2_4*1e6, P3_4*1e5, P4_4*1e5, P5_4*1e5, P6_4*1e5, 0, 0, 0, 0];
    k = [KEXT_4*S/sqrt(ro), KEXT_4*S/sqrt(ro), KEXT_4*S/sqrt(ro), KEXT_4*S/sqrt(ro), KEXT_4*S/sqrt(ro), KEXT_4*S/sqrt(ro), KINT_4*S/sqrt(ro)];
    hg = [HG1_4, HG2_4];
end