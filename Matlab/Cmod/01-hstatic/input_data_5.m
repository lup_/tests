function [p, k, hg, pn, ro, g] = input_data_5()
    global P1_5 P2_5 P3_5 P4_5 P5_5 P6_5;
    global KEXT_5 KINT_5;
    global HG1_5 HG2_5;

    pn = 1e5;
    ro = 1000;
    g = 9.81;
    S = 0.01;

    p = [P1_5*1e6, P2_5*1e6, P3_5*1e5, P4_5*1e5, P5_5*1e5, P6_5*1e5, 0, 0, 0, 0];
    k = [KEXT_5*S/sqrt(ro), KEXT_5*S/sqrt(ro), KEXT_5*S/sqrt(ro), KEXT_5*S/sqrt(ro), KEXT_5*S/sqrt(ro), KEXT_5*S/sqrt(ro), KINT_5*S/sqrt(ro)];
    hg = [HG1_5, HG2_5];
end