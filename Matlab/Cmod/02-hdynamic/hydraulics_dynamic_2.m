global p k hg s pn ro g v;
[p, k, hg, s, pn, ro, g] = input_data_2();
v = zeros(1,7);
h0 = [0; 0];
t = 0:2500;

[T, H] = ode15s(@hydraulics_dynamic_mo_2, t, h0)

ph = figure;
plot(T, H(:,1), T, H(:,2));
xlabel('t');
ylabel('h1, h2');
legend('h1','h2');
saveas(ph, '2.png');