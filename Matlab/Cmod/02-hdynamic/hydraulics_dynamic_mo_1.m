function F = hydraulics_dynamic_mo_1( t, h )
   global p k hg s pn ro g v;

   p(9) = pn * hg(1) / (hg(1) - h(1));
   p(10) = pn * hg(2) / (hg(2) - h(2));
   p(7) = p(9) + ro * g * h(1);
   p(8) = p(10) + ro * g * h(2);

   v(1) = k(1) * sign(p(1) - p(7)) * sqrt(abs(p(1) - p(7)));
   v(2) = k(2) * sign(p(2) - p(7)) * sqrt(abs(p(2) - p(7)));
   v(3) = k(3) * sign(p(3) - p(7)) * sqrt(abs(p(3) - p(7)));
   v(4) = k(4) * sign(p(8) - p(4)) * sqrt(abs(p(8) - p(4)));
   v(5) = k(5) * sign(p(8) - p(5)) * sqrt(abs(p(8) - p(5)));
   v(6) = k(6) * sign(p(8) - p(6)) * sqrt(abs(p(8) - p(6)));
   v(7) = k(7) * sign(p(7) - p(8)) * sqrt(abs(p(7) - p(8)));

   F = [ (v(1) + v(2) + v(3) - v(7)) / s(1); (v(7) - v(4) - v(5) - v(6)) / s(2) ];
end

