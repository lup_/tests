function [p, k, hg, s, pn, ro, g] = input_data_3()
    global P1_3 P2_3 P3_3 P4_3 P5_3 P6_3;
    global KEXT_3 KINT_3;
    global HG1_3 HG2_3;
    global S_3;

    pn = 1e5;
    ro = 1000;
    g = 9.81;
    S = 0.01;

    p = [P1_3*1e6, P2_3*1e6, P3_3*1e6, P4_3*1e6, P5_3*1e6, P6_3*1e5, 0, 0, 0, 0];
    k = [KEXT_3*S/sqrt(ro), KEXT_3*S/sqrt(ro), KEXT_3*S/sqrt(ro), KEXT_3*S/sqrt(ro), KEXT_3*S/sqrt(ro), KEXT_3*S/sqrt(ro), KINT_3*S/sqrt(ro)];
    hg = [HG1_3, HG2_3];
    s = [S_3, S_3];
end