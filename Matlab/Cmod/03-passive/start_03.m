T = FUNC_X_1';
P = FUNC_Y_1';

FTtbl = [
    161.4476  199.5000  215.7073  224.5832  230.1619  233.9860  236.7684  238.8827  240.5433  241.8817;
     18.5128   19.0000   19.1643   19.2468   19.2964   19.3295   19.3532   19.3710   19.3848   19.3959;
     10.1280    9.5521    9.2766    9.1172    9.0135    8.9406    8.8867    8.8452    8.8123    8.7855;
      7.7086    6.9443    6.5914    6.3882    6.2561    6.1631    6.0942    6.0410    5.9988    5.9644;
      6.6079    5.7861    5.4095    5.1922    5.0503    4.9503    4.8759    4.8183    4.7725    4.7351;
      5.9874    5.1433    4.7571    4.5337    4.3874    4.2839    4.2067    4.1468    4.0990    4.0600;
      5.5914    4.7374    4.3468    4.1203    3.9715    3.8660    3.7870    3.7257    3.6767    3.6365;
      5.3177    4.4590    4.0662    3.8379    3.6875    3.5806    3.5005    3.4381    3.3881    3.3472;
      5.1174    4.2565    3.8625    3.6331    3.4817    3.3738    3.2927    3.2296    3.1789    3.1373;
      4.9646    4.1028    3.7083    3.4780    3.3258    3.2172    3.1355    3.0717    3.0204    2.9782;
];

stdExp = std(P);
np = numel(P);

disp('Дисперсия отклонения от среднего: ');
disp(stdExp);

stds = zeros(5,1);
curve1 = fit(T, P, 'exp(a+b/x)', 'StartPoint', [2 10]);
curve2 = fit(T, P, 'exp(a+b/(c+x))', 'StartPoint', [4 2 0.01]);
curve3 = fit(T, P, 'exp(a+b*x+c*x^2)', 'StartPoint', [2 0.001 0.0001]);
curve4 = fit(T, P, 'exp(a+b/x+c*x+d*log(x))', 'StartPoint', [2 1 0.005 0.0001]);
curve5 = fit(T, P, 'exp(a+b*x+c*x^2+d*x^3)', 'StartPoint', [2 0.01 0.0001 0.0000001]);

fp = [2, 3, 3, 4, 4];

for i=1:5
    curveVar = strcat('curve', int2str(i));
    curve=eval(curveVar);
    disp('========');
    disp(strcat('Уравнение #', int2str(i), ': '));
    disp(curve);

    stdModel = sum( (curve(T)-P).^2 )/(np-fp(i));
    stds(i) = stdModel;

    disp('Остаточная дисперсия: ');
    disp(stdModel);

    if (stdExp > stdModel)
        Fr = stdExp/stdModel;
        vmax = np-1;
        vmin = np-fp(i);
    else
        Fr = stdModel/stdExp;
        vmax = np-fp(i);
        vmin = np-1;
    end;

    Ft = FTtbl(vmax,vmin);
    disp('Fr: ');
    disp(Fr);
    disp('Ft: ');
    disp(Ft);
    if Fr > Ft
        disp('Fr > Ft - уравнение адекватно');
    else
        disp('Fr < Ft - уравнение неадекватно');
    end;

    ph = figure;
    plot(T, P, T, curve(T));
    legend('Experiment', 'Model');
    title(int2str(i));
    saveas(ph, strcat('1_',int2str(i),'.png'));

    i = i + 1;
end;

disp('========');
[minStd, k] = min(stds);
disp('Минимальная остаточная дисперсия: ');
disp(minStd);
curveVar = strcat('curve', int2str(k));
curve=eval(curveVar);
disp('Выбранное уравнение: ');
disp(k);
disp(curve);
