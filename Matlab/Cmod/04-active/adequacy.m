function adequacy(thetta, y, np, Phi, Se2, Ft)

    disp('Проверка модели на адекватность');
    yr = 0;
    for i=1:numel(thetta)
        yr = yr + thetta(i)*Phi(:,i);
    end

    Sr2 = sum((y - yr).^2)/(numel(y)-np);

    disp('Дисперсия адекватности:');
    Sr2

    Fr = max(Se2,Sr2)/min(Se2,Sr2);
    disp('Расчетное значение критерия Фишера: ');
    Fr

    if (Fr <= Ft)
        disp(strcat('Fr <= Ft (',num2str(Ft),') - зависимость адекватна'));
    else
        disp(strcat('Fr > Ft (',num2str(Ft),') - зависимость не адекватна'));
    end

end