function [np, thetta_res] =  significancy(z, thetta, Se2, tt)
    np = numel(thetta);
    thetta_res = thetta;

    for i=1:numel(thetta)
        tr = abs( thetta(i) )/sqrt(z(i) * Se2);
        if (tr >= tt)
            disp(strcat('Коэффициент thetta(',int2str(i),') значим'));
        else
            disp(strcat('Коэффициент thetta(',int2str(i),') незначим'));
            thetta_res(i) = 0;
            np = np - 1;
        end
        tr
    end
end