T = DATA_T_1;
t = DATA_t_1;
Cp = DATA_Cp_1;
cplan = [330 75];
delta = [10 25];

%ПФЭ
Phi = [1 -1 -1; 1 1 -1; 1 -1 1; 1 1 1];
thetta = (Phi' * Phi)^(-1) * Phi' * Cp(1:4)';
z = diag((Phi' * Phi)^(-1));

disp('=== Полный факторный эксперимент (ПФЭ) ===');
disp(' ');
disp('Кодированные коэффициенты линейного уравнения: ');
thetta

y = Cp(9:14);
Se2 = sum((y-mean(y)).^2)/5;
disp('Дисперсия воспроизводимости: ');
Se2

tt = 2.57;
[np, thetta] = significancy(z, thetta, Se2, tt);

disp('Итоговые коэффициенты линейной модели');
thetta

adequacy(thetta, Cp(1:4)', np, Phi, Se2, 6.61);

thetta_noenc = [thetta(1) - thetta(2)*(cplan(1)/delta(1)) - thetta(3)*(cplan(2)/delta(2)); thetta(2)/delta(1); thetta(3)/delta(2)];
disp(' ');
disp('Некодированные коэффициенты линейного уравнения: ');
thetta_noenc



%ОЦКП
disp(' ');
disp('=== Ортогональное центральное композиционное планирование (ОЦКП) ===');
s = sqrt(4/numel(Cp))
a = (0.5*4*(sqrt(numel(Cp)/4)-1))^(0.5)

Phi=[1 -1 -1 1 1-s 1-s;
1 1 -1 -1 1-s 1-s;
1 -1 1 -1 1-s 1-s;
1 1 1 1 1-s 1-s;
1 -a 0 0 a^2-s -s;
1 a 0 0 a^2-s -s;
1 0 -a 0 -s a^2-s;
1 0 a 0 -s a^2-s;
1 0 0 0 -s -s;
1 0 0 0 -s -s;
1 0 0 0 -s -s;
1 0 0 0 -s -s;
1 0 0 0 -s -s;
1 0 0 0 -s -s];
thetta = (Phi' * Phi)^(-1) * Phi' * Cp';
z = diag((Phi' * Phi)^(-1));

disp('Кодированные коэффициенты нелинейного уравнения: ');
thetta

[np, thetta] = significancy(z, thetta, Se2, tt);
disp('Итоговые коэффициенты нелинейной модели');
thetta

adequacy(thetta, Cp', np, Phi, Se2, 2.8);

T_c = cplan(1);
t_c = cplan(2);
dT = delta(1);
dt = delta(2);

thetta_noenc = [ thetta(1) - thetta(2)*(T_c/dT) - thetta(3)*(t_c/dt) + thetta(4)*T_c*t_c/(dT*dt) + thetta(5)*T_c^2/dT^2 - thetta(5)*s+ thetta(6)*t_c^2/dt^2 - thetta(6)*s;
thetta(2)/dT-thetta(4)*t_c/(dT*dt)-2*thetta(5)*T_c/dT^2;
thetta(3)/dt-thetta(4)*T_c/(dT*dt)-2*thetta(6)*t_c/dt^2;
thetta(4)/(dT*dt);
thetta(5)/dT^2;
thetta(6)/dt^2;
];

disp(' ');
disp('Некодированные коэффициенты нелинейного уравнения: ');
thetta_noenc

A=[2*thetta(5) thetta(4);thetta(4) 2*thetta(6)];
b=[-thetta(2);-thetta(3)];
zopt = A\b;

disp('Кодированные координаты оптимальной точки: ');
zopt

xopt = [dT*zopt(1)+T_c; dt*zopt(2)+t_c];

disp('Действительные координаты оптимальной точки: ');
disp(sprintf('T_opt = %G К, t_opt = %G с', xopt(1), xopt(2)));