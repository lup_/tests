warning('off', 'all');
global n0 L;

[l, n] = ode15s(@react_mo, [0 L], n0);

N = sum(n, 2);
x = n./N;

na = n(:,1);
n0a = na(1);
nka = na(numel(na));

gamma = (n0a-nka)/n0a * 100;

x
fprintf('gamma(A) = %G', gamma);

ph = figure;
hold on
for i = 1:numel(x(1, :))
    plot(l, x(:,i));
end
xlabel('l');
ylabel('x');
legend('xA','xB', 'xC', 'xD', 'xE');
saveas(ph, '1.png');