function dx = react_mo (l, n)
    global alpha r Vr L p0;

    N = sum(n);
    p = p0 .* n./N;
    g = alpha * r(p);

    dx = Vr/L .* g;
end