%1
x = -10:0.1:10;
y = -A_1.*x.^3 - B_1.*x.^2 + C_1.*x + D_1;
ph = plot(x, y);
grid on;
saveas(ph, '1.png');

%2
x = -10:0.1:10;
y = -A_2.*x.^5 + B_2.*x.^4 + C_2.*x.^3 - D_2.*x.^2 - E_2.*x + F_2;
ph = plot(x, y);
grid on;
saveas(ph, '2.png');

%3
x = -10:0.1:10;
y = A_3.*( sin(B_3.*x)./x );
ph = plot(x, y);
grid on;
saveas(ph, '3.png');
