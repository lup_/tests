%1
[x, y] = meshgrid(-10:0.5:10, -10:0.5:10);
z = A_1.*x.^2 - B_1.*y.^2;
ph = figure;
surf(x, y, z);
saveas(ph, '1.png');

%2
[x, y] = meshgrid(-10:0.5:10, 0:0.5:20);
z = sqrt(A_2.*y).*(x.^2+B_2);
ph = figure;
surf(x, y, z);
saveas(ph, '2.png');

%3
[x, y] = meshgrid(-5:0.2:5, -5:0.2:5);
z = C_3.*exp(-(x-A_3).^2 - (y-B_3).^2);
ph = figure;
surf(x, y, z);
saveas(ph, '3.png');

%4
[x, y] = meshgrid(-5:0.5:5, -5:0.5:5);
z = (A_4.*x.^2 + B_4.*y.^2)./C_4;
ph = figure;
surf(x, y, z);
saveas(ph, '4.png');