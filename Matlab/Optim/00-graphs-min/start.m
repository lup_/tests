%1
x = -10:0.1:10;
y = ( (A_1+B_1.*x).*(C_1.*x.^2+D_1.*x-E_1) ).^(1/3);
ph = plot(x, y);
saveas(ph, '1.png');

%2
x = -10:0.1:10;
y = (A_2.*x-B_2)./(sqrt(C_2.*x.^2-D_2));
ph = plot(x, y);
saveas(ph, '2.png');

%3
x = -10:0.1:10;
y = (A_3.*x.^3+B_3.*x.^2-C_3.*x-D_3)./(E_3.*x.^2-F_3);
ph = plot(x, y);
saveas(ph, '3.png');


%4
[x, y] = meshgrid(-10:0.5:10, -10:0.5:10);
z = A_4.*x.^2 - B_4.*y.^2;
ph = figure;
surf(x, y, z);
saveas(ph, '4.png');

%5
disp('=== 5');
f = @(x) A_5.*x.^4-B_5.*x.^2+C_5;
[x1, y1] = fminbnd(f, -10, 0);
[x2, y2] = fminbnd(f, 0, 10);
disp('Локальные минимумы функции:');
disp([x1 y1]);
disp([x2 y2]);
disp('===');