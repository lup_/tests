function x = reactXTime (T, A, E, t, x0, reactCoeffs, rx)

  global A;
  global E;
  global t;
  global x0;
  
  R = 1.987;
  k = A.*exp(-E./(R.*T));

  compCount = numel(reactCoeffs(:,1));
  
  r = k * t;
  rdiag = ( r * ones(1, compCount) ).*rx';
  Am = -(reactCoeffs * rdiag - eye(compCount));
  x = Am\x0;
end