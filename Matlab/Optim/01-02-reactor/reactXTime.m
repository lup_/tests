function x = reactXTime (t, k, x0, reactCoeffs, rx)
  compCount = numel(reactCoeffs(:,1));
  
  r = k * t;
  rdiag = ( r * ones(1, compCount) ).*rx';
  A = -(reactCoeffs * rdiag - eye(compCount));
  x = A\x0;
end