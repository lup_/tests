warning('off', 'all');
A = [A1_3; A2_3; A3_3]; % мин^-1
E = [E1_3; E2_3; E3_3]; % кал/моль
t = [TAU_3]; % мин
x0 = [XA_3; 0; 0; 0]; % мольные доли

reactCoeffs = [
  % r1      r2     r3
    -1      0      0     ; % A
     KPA_3  -1     -1    ; % P
     KBA_3  0      KBP_3 ; % B
     0      KS_3   0     ; % S
  ];

rx = [
  %  r1   r2   r3
     1    0    0 ; % A
     0    1    1 ; % P
     0    0    0 ; % B
     0    0    0 ; % S
  ];

colors = ['k','r','g','b','m','c'];

xtotal = [];
Trange = 100:0.5:1000;
for T = Trange
  xtotal = cat(2, xtotal, reactXTemp(T, A, E, t, x0, reactCoeffs, rx));
end;

getel = @(array, n) array(n);
xpt = @(T) -getel(reactXTemp(T, A, E, t, x0, reactCoeffs, rx), 2);

optim = fminbnd(xpt, Trange(1), Trange(numel(Trange)));
optim

compCount = numel(xtotal(:,1));

close;
ph = figure;
hold on;
for n = 1:compCount
  colorNum = rem(n, numel(colors))+1;
  plot(Trange, xtotal(n,:), colors(colorNum));
end;
plot(optim, -xpt(optim), 'ro');
xlabel('T, К');
ylabel('x, м.д.');
legend('A', 'P', 'B', 'S');
saveas(ph, '3.png');
hold off;