warning('off', 'all');
A = [A1_5; A2_5; A3_5; A4_5]; % мин^-1
E = [E1_5; E2_5; E3_5; E4_5]; % кал/моль
t = [TAU_5]; % мин
x0 = [XA_5; 0; 0]; % мольные доли

reactCoeffs = [
  % r1      r2     r3     r4
    -1      1      0      0   ; % A
     1     -1     -1      1   ; % P
     0      0      1      -1  ; % S
  ];

rx = [
  %  r1   r2   r3   r4
     1    0    0    0  ; % A
     0    1    1    0  ; % P
     0    0    0    1  ; % S
  ];

colors = ['k','r','g','b','m','c'];

xtotal = [];
Trange = 100:0.5:1000;
for T = Trange
  xtotal = cat(2, xtotal, reactXTemp(T, A, E, t, x0, reactCoeffs, rx));
end;

getel = @(array, n) array(n);
xpt = @(T) -getel(reactXTemp(T, A, E, t, x0, reactCoeffs, rx), 2);

optim = fminbnd(xpt, Trange(1), Trange(numel(Trange)));
optim

compCount = numel(xtotal(:,1));

close;
ph = figure;
hold on;
for n = 1:compCount
  colorNum = rem(n, numel(colors))+1;
  plot(Trange, xtotal(n,:), colors(colorNum));
end;
plot(optim, -xpt(optim), 'ro');
xlabel('T, К');
ylabel('x, м.д.');
legend('A', 'P', 'S');
saveas(ph, '5.png');
hold off;