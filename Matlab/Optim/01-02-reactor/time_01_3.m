k = [K1_3; K2_3; K3_3; K4_3];
x0 = [XA_3; 0; 0; 0];

reactCoeffs = [
  % r1     r2     r3    r4
    -1     1      0     0      ; % A
     1     -1     -1    1      ; % P
     1     -1     0     0      ; % B
     0     0      1     -1     ; % S
  ];

rx = [
  %  r1   r2   r3   r4
     1    0    0    0  ; % A
     0    1    1    0  ; % P
     0    1    0    0  ; % B
     0    0    0    1  ; % S
  ];

colors = ['k','r','g','b','m','c'];

xtotal = [];
trange = 0:0.1:100;
for t = trange
  xtotal = cat(2, xtotal, reactXTime(t, k, x0, reactCoeffs, rx));
end;

getel = @(array, n) array(n);
xpt = @(t) -getel(reactXTime(t, k, x0, reactCoeffs, rx), 2);

optim = fminbnd(xpt, 0, 100);
optim

compCount = numel(xtotal(:,1));

close;
ph = figure;
hold on;
for n = 1:compCount
  colorNum = rem(n, numel(colors))+1;
  plot(trange, xtotal(n,:), colors(colorNum));
end;
plot(optim, -xpt(optim), 'ro');
hold off;
xlabel('t, час');
ylabel('x, м.д.');
legend('A', 'P', 'B', 'S');
saveas(ph, '3.png');