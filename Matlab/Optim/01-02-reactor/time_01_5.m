k = [K1_5; K2_5; K3_5];
x0 = [XA_5; 0; XB_5; 0; 0;];

reactCoeffs = [
  % r1      r2     r3
    -1      0      0     ; % A
     KPA_5  KPB_5  -1    ; % P
     KB_5   -1     0     ; % B
     0      KD_5   0     ; % D
     0      0      KS_5  ; % S
  ];

rx = [
  %  r1   r2   r3
     1    0    0  ; % A
     0    0    1  ; % P
     0    1    0  ; % B
     0    0    0  ; % D
     0    0    0  ; % S
  ];

colors = ['k','r','g','b','m','c'];

xtotal = [];
trange = 0:0.1:50;
for t = trange
  xtotal = cat(2, xtotal, reactXTime(t, k, x0, reactCoeffs, rx));
end;

getel = @(array, n) array(n);
xpt = @(t) -getel(reactXTime(t, k, x0, reactCoeffs, rx), 2);

optim = fminbnd(xpt, 0, 50);
optim

compCount = numel(xtotal(:,1));

close;
ph = figure;
hold on;
for n = 1:compCount
  colorNum = rem(n, numel(colors))+1;
  plot(trange, xtotal(n,:), colors(colorNum));
end;
plot(optim, -xpt(optim), 'ro');
hold off;
xlabel('t, час');
ylabel('x, м.д.');
legend('A', 'P', 'B', 'D', 'S');
saveas(ph, '5.png');