k = [K1_1; K2_1];
x0 = [XA_1; 0; 0];

mathdesc = @(t, x) [
                      % r1     r2
                        -1     0    ; % A
                         KP_1  -1   ; % P
                         0     KS_1 ; % S
                   ] * [
                        k(1)*x(1)      ; % r1
                        k(2)*x(2)      ; % r2
                      ];

periodic_main(k, x0, 2, mathdesc, 1);