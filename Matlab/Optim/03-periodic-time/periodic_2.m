k = [K1_2; K2_2; K3_2];
x0 = [XA_2; 0; 0];

mathdesc = @(t, x) [
                      % r1     r2    r3
                        -1     1     0     ; % A
                         1     -1    -1    ; % P
                         0     0     KS_2  ; % S
                   ] * [
                        k(1)*x(1)      ; % r1
                        k(2)*x(2)      ; % r2
                        k(3)*x(2)      ; % r3
                      ];

periodic_main(k, x0, 2, mathdesc, 2);