k = [K1_3; K2_3; K3_3; K4_3];
x0 = [XA_3; 0; 0;];

mathdesc = @(t, x) [
                      % r1     r2     r3    r4
                        -1     1      0     0      ; % A
                         1     -1     -1    1      ; % P
                         0     0      1     -1     ; % S
                   ] * [
                        k(1)*x(1)      ; % r1
                        k(2)*x(2)      ; % r2
                        k(3)*x(2)      ; % r3
                        k(4)*x(3)      ; % r4
                      ];

periodic_main(k, x0, 2, mathdesc, 3);