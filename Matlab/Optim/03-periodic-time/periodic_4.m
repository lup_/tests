k = [K1_4; K2_4; K3_4];
x0 = [XA_4; 0; XB_4; 0];

mathdesc = @(t, x) [
                      % r1      r2     r3
                        -1      0      0     ; % A
                         KPA_4  KPB_4  -1    ; % P
                         0      -1     0     ; % B
                         0      0      KS_4  ; % S
                   ] * [
                        k(1)*x(1)      ; % r1
                        k(2)*x(3)      ; % r2
                        k(3)*x(2)      ; % r3
                      ];

periodic_main(k, x0, 2, mathdesc, 4);