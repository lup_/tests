k = [K1_5; K2_5; K3_5];
x0 = [XA_5; 0; XB_5; 0; 0;];

mathdesc = @(t, x) [
                      % r1      r2     r3
                        -1      0      0     ; % A
                         KPA_5  KPB_5  -1    ; % P
                         KB_5   -1     0     ; % B
                         0      KD_5   0     ; % D
                         0      0      KS_5  ; % S
                   ] * [
                        k(1)*x(1)      ; % r1
                        k(2)*x(3)      ; % r2
                        k(3)*x(2)      ; % r3
                      ];

periodic_main(k, x0, 2, mathdesc, 5);