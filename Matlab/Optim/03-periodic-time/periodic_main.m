function periodic_main(k, x0, xindex, mathdesc, var)

    ph = figure;
    ode45(mathdesc,[0;100],x0);
    saveas(ph, strcat(int2str(var), '_1.png'));

    [T,X]=ode45(mathdesc,[0;100],x0);
    xp = X(:,xindex);
    xpoptim = fit(T, xp, 'a*x/(b*x^c+d)', 'StartPoint', [1 1 1 1]);

    ph = figure;
    plot(T,xp, T, xpoptim(T));
    saveas(ph, strcat(int2str(var), '_2.png'));

    Topt = fminbnd(@(x) -xpoptim(x), 0, 100);
    [xpopt,index]=max(xp);
    Tmax = T(index);

    disp(sprintf('Максимальное значение в результатах интегрирования: %G К', Tmax));
    disp(sprintf('Оптимальное значение, полученное при помощи аппроксимации: %G К', Topt));

end