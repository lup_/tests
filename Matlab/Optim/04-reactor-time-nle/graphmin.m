function graphmin(startx, compPnum, mathdesc, varnum)

    trange = 1:100;
    X = [];

    for t=trange
        X = [X, nlesolve(t, startx, mathdesc)];
    end

    tmax = fminbnd(@(t) -nlesolvecomp(t, startx, mathdesc, compPnum), min(trange), max(trange));
    xmax = nlesolvecomp(tmax, startx, mathdesc, compPnum);

    ph = figure;
    hold on;
    for index=1:length(startx)
        plot(trange, X(index, :));
    end
    plot(tmax,xmax,'ro');
    saveas(ph, strcat(int2str(varnum), '.png'));

    tmax
end