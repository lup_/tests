function res = nlesolve(t, startx, mathdesc)
    options = optimset('Display','off');

    res = fsolve(@(x) mathdesc(t,x), startx, options);

end