function res = nlesolvecomp(t, startx, mathdesc, compn)

    solution = nlesolve(t, startx, mathdesc);
    res = solution(compn);

end