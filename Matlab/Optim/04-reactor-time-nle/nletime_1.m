k = [K1_1; K2_1];
x0 = [XA_1; 0; 0; 0];
compPnum = 2;
varnum = 1;

mathdesc = @(t, x) x0 - x + t.* [
                    % r1     r2
                      -KA_1  0      ; % A
                       KP_1  -KP_1  ; % P
                       KB_1  -KB_1  ; % B
                       0     1      ; % S
                   ] * [
                      k(1)*x(1)^KA_1                ; % r1
                      k(2)*x(2)^KP_1*x(3)^KB_1      ; % r2
                   ];


graphmin(x0, compPnum, mathdesc, varnum);

