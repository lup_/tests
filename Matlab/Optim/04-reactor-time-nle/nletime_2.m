k = [K1_2; K2_2; K3_2];
x0 = [XA_2; 0; XB_2; 0];
compPnum = 2;
varnum = 2;

mathdesc = @(t, x) x0 - x + t.* [
                    % r1     r2     r3
                      -KA_2  KA_2   0      ; % A
                       KP_2  -KP_2  -1     ; % P
                       0     0      -KB_2  ; % B
                       0     0      KS_2   ; % S
                   ] * [
                      k(1)*x(1)^KA_2           ; % r1
                      k(2)*x(2)^KP_2           ; % r2
                      k(3)*x(2)*x(3)^KB_2      ; % r3
                   ];


graphmin(x0, compPnum, mathdesc, varnum);

