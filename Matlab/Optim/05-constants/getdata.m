function [tmin, tspan, xexpmin] = getdata(texp, xexp)
    tmin = texp(1);
    xexpmin = xexp(1, :);
    tspan = texp(2:length(texp))';
end
