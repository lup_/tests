function criteria = kinetic(k, texp, xexp)
    xcalc = [];
    [tmin, tspan, xexpmin] = getdata(texp, xexp);
    xcalc = mdsolve(tmin, tspan, xexpmin, k);

    criteria = sum( sum((xexp-xcalc).^2) );
end