t = REACT_X_1';
xa = REACT_Y0_1';
xp = REACT_Y1_1';

x = [xa xp];
k0 = [0.1 1];

[kcalc, critmin] = fminsearch(@(k) kinetic(k, t, x), k0)

xcount = length(x(1,:));
[tmin, tspan, xexpmin] = getdata(t, x);
xcalc = mdsolve(tmin, tspan, xexpmin, kcalc)

ph = figure;
hold on;
for index=1:xcount
    plot(t,x(:,index));
    plot(t,xcalc(:,index),'*--');
end;
hold off;
legend('xA эксп.', 'xA мод.', 'xP эксп.', 'xP мод.');
saveas(ph, '1.png');