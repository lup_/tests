function res = mdreact(t, x, k)
    res = [
        -k(1)*x(1);
        k(1)*x(1) - k(2)*x(2);
    ];
end