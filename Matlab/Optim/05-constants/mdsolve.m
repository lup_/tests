function res = mdsolve(tmin, tspan, x0, k)
    xcalc = [];

    for tcurr = tspan
        [tcalc, X] = ode45(@(t, x) mdreact(t,x,k), [tmin tcurr], x0);
        if ( isempty(xcalc) )
            xcalc = X(1, :);
        end        
        Xt = X(length(X), :);
        xcalc = [xcalc; Xt];
    end
    
    res = xcalc; 
end