T0 = 300;
t0 = 0.5;
x0 = [XA_1 0 0 0];
compNum = 3;
A = [A1_1 A2_1 A3_1 A4_1];
E = [E1_1 E2_1 E3_1 E4_1];

md = @(t, k, x) [
                    % r1     r2     r3     r4
                      -1     -1     0      0      ; % A
                       KB_1  0      -1     0      ; % B
                       0     0      KP_1   -1     ; % P
                       0     KD_1   0      1      ; % D
                   ] * [
                      k(1)*x(1)        ; % r1
                      k(2)*x(1)        ; % r2
                      k(3)*x(2)        ; % r3
                      k(4)*x(3)        ; % r4
                   ];

[indetmin, zval] = fminsearch(@(indet) -mddouble(indet, md, compNum, x0, A, E), [T0 t0]);
fprintf('Оптимальное значение T, К: %G\n', indetmin(1));
fprintf('Оптимальное значение t, сек: %G\n', indetmin(2));

[T, t] = meshgrid(100:10:2000, 0.1:0.05:3);
z = [];
Tindex = 1;
for Tcurr = T(1,:)
    tindex = 1;
    for tcurr = t(:,1)'
        z(tindex, Tindex) = mddouble([Tcurr tcurr], md, compNum, x0, A, E);
        tindex = tindex + 1;
    end
    Tindex = Tindex + 1;
end

ph = figure;
surf(T, t, z);
xlabel('T, К');
ylabel('t, сек');
zlabel('Xp, м.д.');
saveas(ph, '1.png');