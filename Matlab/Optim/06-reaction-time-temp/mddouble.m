function res = mddouble(indet, md, compNum, x0, A, E) 
    T = indet(1);
    t = indet(2);
    
    R = 1.987;  
    k = A .* exp( (E./R).*(1./T - 1/(658+273)) );
    
    [tcalc, Xcalc] = ode45(@(t, x) md(t, k, x), [0 t], x0);
    lastIndex = length(tcalc);
    xp = Xcalc(lastIndex, 3);
    res = xp;
end