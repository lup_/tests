function cr = criteria(nu, Cp, T0, T, nux, Cpx, T0x, KT, Cx, Cf)
    options = optimset('Display','off');

    Tx0 = 100;
    FT0 = 100;
    x = fsolve(@(x) md(x, nu, Cp, T0, T, nux, Cpx, T0x, KT), [Tx0 FT0], options);
    
    Tx = x(1);
    FT = x(2);

    %Tx = T0x + (T0 - T) * (nu * Cp) / (nux * Cpx);
    %FT = nu * Cp * (T - T0) / ( KT * (Tx - T) );
    
    cr = Cx * nux + Cf * FT;
end