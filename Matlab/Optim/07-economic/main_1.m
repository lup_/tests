nu = NU_1;
Cp = CP_1;
T0 = T0_1;
T = T_1;

Cpx = CPX_1;
T0x = T0X_1;

KT = KT_1;
Cx = CX_1;
Cf = CF_1;

nuxrange = 4:0.1:15;

[nuxmin, crmin] = fminbnd(@(nux) criteria(nu, Cp, T0, T, nux, Cpx, T0x, KT, Cx, Cf), min(nuxrange), max(nuxrange))

crit = [];
for nuxc=nuxrange
    cr = criteria(nu, Cp, T0, T, nuxc, Cpx, T0x, KT, Cx, Cf);
    crit = [crit cr];
end

ph = figure;
plot(nuxrange, crit, nuxmin, crmin, 'ro-');
saveas(ph, '1.png');