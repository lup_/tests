function res = md(x, nu, Cp, T0, T, nux, Cpx, T0x, KT)
    Tx = x(1);
    FT = x(2);

    res = [
        nu * Cp * (T0 - T) + nux * Cpx * (T0x - Tx);
        nu * Cp * (T0 - T) + KT * FT * (Tx - T);
    ];
end