function balanceLeft = L_balance(L, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_cold0)   
    [l, T_cold_l] = ode45(@(l, T_cold) T_cold_diff(D, Kt, T_hot, T_cold, nu_cold, Cp_cold, l, L), [0 L], [T_cold0]);
    last = length(l);
    T_cold = T_cold_l(last);  
    
    balanceLeft = nu_hot * Cp_hot * (T_hot0 - T_hot) + nu_cold * Cp_cold * (T_cold0 - T_cold);
end