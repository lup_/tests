function dT = T_cold_diff(D, Kt, T_hot, T_cold, nu_cold, Cp_cold, dl, L)
    dT = (pi * D * Kt * (T_hot - T_cold) * dl) / (nu_cold * Cp_cold * L);
end