function L = getL(D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_cold0)
    Lstart = 80;
    L = fzero(@(L) L_balance(L, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_cold0), Lstart);
end