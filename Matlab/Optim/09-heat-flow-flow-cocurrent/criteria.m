function cr = criteria(Cx, Cf, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_cold0)
    L = getL(D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_cold0);
    Ft = pi * D * L;

    cr = Cx * nu_cold + Cf * Ft;
end