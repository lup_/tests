T_hot0 = T0_1;
T_hot = T_1;
nu_hot = NU_1;
Cp_hot = CP_1;

Cp_cold = CPX_1;
T_cold0 = T0X_1;

Kt = KT_1;
D = D_1;

Cx = CX_1;
Cf = CF_1;

L = 80; %приближение
nu_cold_range = [1 15];

[nu_cold, Cr] = fminbnd(@(nu_cold) criteria(Cx, Cf, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_cold0), nu_cold_range(1), nu_cold_range(2));

nu_cold
Cr