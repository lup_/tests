function balanceLeft = L_balance(L, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_coldL)
    T_cold0 = getT_cold0(L, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_coldL);

    [l, T_l] = ode45(@(l, T) T_cold_T_hot_diff(D, Kt, T, nu_cold, Cp_cold, nu_hot, Cp_hot, l, L), [0 L], [T_cold0 T_hot0]);
    last = length(l);
    T_hot_calc = T_l(last, 2);
    
    balanceLeft = T_hot_calc - T_hot;
end