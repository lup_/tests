function balanceLeft = L_balance_cold(L, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_cold0, T_coldL)
    [l, T_l] = ode45(@(l, T) T_cold_T_hot_diff(D, Kt, T, nu_cold, Cp_cold, nu_hot, Cp_hot, l, L), [0 L], [T_cold0 T_hot0]);
    last = length(l);
    T_coldL_calc = T_l(last, 1);
    
    balanceLeft = T_coldL_calc - T_coldL;
end