function dT = T_cold_T_hot_diff(D, Kt, T, nu_cold, Cp_cold, nu_hot, Cp_hot, dl, L)
    T_cold = T(1);
    T_hot = T(2);

    dT_cold = (pi * D * Kt * (T_cold - T_hot) * dl) / (nu_cold * Cp_cold * L);
    dT_hot = (pi * D * Kt * (T_cold - T_hot) * dl) / (nu_hot * Cp_hot * L);
    
    dT = [dT_cold; dT_hot];
end