function cr = criteria(Cx, Cf, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_coldL)
    L = getL(D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_coldL);
    Ft = pi * D * L;

    cr = Cx * nu_cold + Cf * Ft;
end