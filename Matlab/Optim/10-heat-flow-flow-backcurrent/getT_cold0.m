function T_cold0 = getT_cold0(L, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_coldL)
    T_cold0_start = 80;
    T_cold0 = fzero(@(T_cold0) L_balance_cold(L, D, Kt, nu_hot, Cp_hot, T_hot0, T_hot, nu_cold, Cp_cold, T_cold0, T_coldL), T_cold0_start);
end