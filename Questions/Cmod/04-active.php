<?php
return [
    "0" => [
        "text" => "Чем активный эксперимент отличается от пассивного?",
        "type" => "single",
        "answers" => [
            0 => "Активный эксперимент проводится с большей скоростью, чем пассивный",
            1 => "Активный эксперимент проводится в условиях реального химического производства, а пассивный - в лаборатории",
            2 => "Активный эксперимент проводится по плану, а пассивный эксперимент является простым наблюдением",
            3 => "В активном эксперименте исследователь может изменять условия его проведения, а в пассивном - нет",
        ],
        "correct" => 2,
    ],
    "1" => [
        "text" => "Что такое \"факторы\" в рамках активного эксперимента?",
        "type" => "single",
        "answers" => [
            0 => "Выходные значения факторного пространства",
            1 => "Входные переменные факторного пространства",
            2 => "Выходные значения функции отклика",
            3 => "Входные переменные функции отклика",
        ],
        "correct" => 3,
    ],
    "2" => [
        "text" => "Сколько опытов нужно провести в ПФЭ, если число факторов равно 4?",
        "type" => "single",
        "answers" => [
            0 => "16",
            1 => "24",
            2 => "30",
            3 => "8",
        ],
        "correct" => 0,
    ],
    "3" => [
        "text" => "Сколько опытов проводится при ОЦКП, если число факторов равно 3 (а опытов в центре плата - 6)?",
        "type" => "single",
        "answers" => [
            0 => "8",
            1 => "14",
            2 => "20",
            3 => "82",
        ],
        "correct" => 2,
    ],
    "4" => [
        "text" => "Чем обусловлена схема выбора точек для ОЦКП?",
        "type" => "single",
        "answers" => [
            0 => "Большим числом коэффициентов в модели второго порядка",
            1 => "Условием ортогональности матрицы планирования",
            2 => "Большим числом коэффициентов в модели второго порядка и условием ортогональности матрицы планирования",
            3 => "Ни одним из перечисленных условий",
        ],
        "correct" => 2,
    ],
    "5" => [
        "text" => "Выберите формулу для вычисления \"звездного плеча\"",
        "type" => "single",
        "answers" => [
            0 => "`alpha = sqrt( -frac(n)(2) + frac( sqrt(Nn) )(2) )`",
            1 => "`alpha = sqrt( -frac(Nn)(2) + frac( sqrt(n) )(2) )`",
            2 => "`S = sqrt( frac(n)(N) )`",
            3 => "`S = sqrt( frac(N)(n) )`",
        ],
        "correct" => 0,
    ],
    "6" => [
        "text" => "Какое минимальное количество опытов нужно проводить в центре плана?",
        "type" => "single",
        "answers" => [
            0 => "0",
            1 => "1",
            2 => "2",
            3 => "6",
        ],
        "correct" => 2,
    ],
    "7" => [
        "text" => "Почему в ПФЭ применяется линейная модель?",
        "type" => "single",
        "answers" => [
            0 => "Потому что погрешности определения значений выходной переменной незначительны",
            1 => "Потому что данные получены вдали от экстремального значения выходной переменной",
            2 => "Потому что любая другая модель будет не адекватной",
            3 => "Потому что у этой модели минимальное значение остаточной дисперсии",
        ],
        "correct" => 1,
    ],
    "8" => [
        "text" => "Укажите ложное высказывание:",
        "type" => "single",
        "answers" => [
            0 => "Коэффициенты полученного регрессионного уравнения статистически не зависимы",
            1 => "Корреляционная матрица является диагональной",
            2 => "Информационная матрица не является диагональной",
            3 => "Матрица планирования является симметричной, ортогональной и нормированной;",
        ],
        "correct" => 2,
    ],
    "9" => [
        "text" => "Укажите верную формулу для определения коэффициентов регрессии в ПФЭ:",
        "type" => "single",
        "answers" => [
            0 => "`tilde a_j = frac(sum_(i=1)^n z_iy_j)(4)`",
            1 => "`tilde a_j = frac(sum_(i=1)^n z_(ij)y_(ij))(4)`",
            2 => "`tilde a_j = frac(sum_(i=1)^n z_iy_(ij))(4)`",
            3 => "`tilde a_j = frac(sum_(i=1)^n z_(ij)y_i)(4)`",
        ],
        "correct" => 3,
    ],
    "10" => [
        "text" => "Укажите верный вид регрессии в кодированных переменных, используемой в ОЦКП",
        "type" => "single",
        "answers" => [
            0 => "`hat y = tilde(a)_0 z_0 + tilde(a)_1 z_1 + tilde(a)_2 z_2 + tilde(a)_12 z_1 z_2 + tilde(a)_11 (z_1^2 - S) + tilde(a)_22 (z_2^2 - S)`",
            1 => "`hat y = tilde(a)_0 z_0 + tilde(a)_1 z_1 + tilde(a)_2 z_2`",
            2 => "`C_p = Theta_0 + Theta_1 T + Theta_2 tau + Theta_12 T tau + Theta_11 T^2 + Theta_22 tau^2`",
            3 => "`C_p = Theta_0 + Theta_1 T + Theta_2 tau`",
        ],
        "correct" => 0,
    ],
    "11" => [
        "text" => "О чем <b>НЕ</b> может свидетельствовать неадекватность модели, полученной на первом этапе метода Бокса-Вильсона?",
        "type" => "single",
        "answers" => [
            0 => "О невозможности применения линейной модели к данному эксперименту в целом",
            1 => "О существенных погрешностях измерения",
            2 => "О возможной близости к точке экстремума",
            3 => "Об отдалении от точки экстремума",
        ],
        "correct" => 3,
    ],
    "12" => [
        "text" => "На какие этапы делится эксперимент, проводимый по методу Бокса-Вильсона?",
        "type" => "single",
        "answers" => [
            0 => "<ol>
                    <li>Планирование исходных опытов так, чтобы экспериментальные точки образовывали симплекс</li>
                    <li>Построение нового симплекса на базе наихудшей точки предыдущего</li>
                    <li>Продолжение уточнения до достижения оптимальной точки</li>
                  </ol>",
            1 => "<ol>
                    <li>Движение к \"почти стационарной области\"</li>
                    <li>Уточнение экстремума в этой области</li>
                  </ol>",
            2 => "<ol>
                    <li>Случайным образом выбираются исходные экспериментальные точки</li>
                    <li>Проводятся уточняющие эксперименты в направлении градиента</li>
                    <li>Продолжение уточнения до достижения оптимальной точки</li>
                  </ol>",
            3 => "<ol>
                    <li>Проведение исходного опыта в центре плана</li>
                    <li>Поочередное варьирование каждого из факторов до достижения оптимальной точки</li>
                  </ol>",
        ],
        "correct" => 1,
    ],
    "13" => [
        "text" => "Укажите метод, используемый для нахождения коэффициентов регрессии в рамках метода Бокса-Вильсона",
        "type" => "single",
        "answers" => [
            0 => "Метод наименьших квадратов (МНК)",
            1 => "Метод наименьших модулей (МНМ)",
            2 => "Метод инструментальных переменных (ИП)",
            3 => "Метод максимального правдоподобия (ММП)",
        ],
        "correct" => 0,
    ],
    "14" => [
        "text" => "В рамках метода Бокса-Вильсона ПФЭ проводится",
        "type" => "single",
        "answers" => [
            0 => "Строго однократно",
            1 => "Один или два раза, в зависимости от адекватности полученной модели",
            2 => "Многократно, до достижения почти стационарной области",
            3 => "Не проводится",
        ],
        "correct" => 2,
    ],
    "15" => [
        "text" => "При каком условии рекомендуется повторить ОЦКП в новом центре плана?",
        "type" => "single",
        "answers" => [
            0 => "`|z_j^(opt)| > 1` `(j=1-:m)`",
            1 => "`|z_j^(opt)| > 1` `(j=1-:m)` и/или полученное уравнение регрессии не является адекватным",
            2 => "Полученное уравнение регрессии не является адекватным",
            3 => "Повторные эксперименты по ОЦКП не проводятся",
        ],
        "correct" => 1,
    ],
    "16" => [
        "text" => "Выберите верную формулу для определения расчетного значения коэффициента Стьюдента при определении значимости коэффициентов регрессии ПФЭ",
        "type" => "single",
        "answers" => [
            0 => "`hat(t)_(a_j) = frac( |a_j| )( sqrt(S_e^2 ) )`",
            1 => "`hat(t)_(a_j) = frac( |a_j| )( sqrt(C_(jj)S_e^2 ) )`",
            2 => "`hat(t)_(a_j) = frac( |a_j| )( sqrt(S_r^2 ) )`",
            3 => "`hat(t)_(a_j) = frac( |a_j| )( sqrt(C_(jj)S_r^2 ) )`",
        ],
        "correct" => 1,
    ],
    "17" => [
        "text" => "Выберите верную формулу для кодирования факторов",
        "type" => "single",
        "answers" => [
            0 => "`z_j = frac(x_j - x_j^(min))(x_j^(max))`",
            1 => "`z_j = frac(x_j^\"(0)\")(Delta x_j)`",
            2 => "`z_j = frac(x_j)(Delta x_j)`",
            3 => "`z_j = frac(x_j - x_j^\"(0)\")(Delta x_j)`",
        ],
        "correct" => 3,
    ],
    "18" => [
        "text" => "Укажите верный вид регрессии в кодированных переменных, используемой в ПФЭ",
        "type" => "single",
        "answers" => [
            0 => "`hat y = tilde(a)_0 z_0 + tilde(a)_1 z_1 + tilde(a)_2 z_2 + tilde(a)_12 z_1 z_2 + tilde(a)_11 (z_1^2 - S) + tilde(a)_22 (z_2^2 - S)`",
            1 => "`hat y = tilde(a)_0 z_0 + tilde(a)_1 z_1 + tilde(a)_2 z_2`",
            2 => "`C_p = Theta_0 + Theta_1 T + Theta_2 tau + Theta_12 T tau + Theta_11 T^2 + Theta_22 tau^2`",
            3 => "`C_p = Theta_0 + Theta_1 T + Theta_2 tau`",
        ],
        "correct" => 1,
    ],
    "19" => [
        "text" => "Укажите условие, по которому определяют достижение \"почти стационарной области\" и необходимость проведения ОЦКП",
        "type" => "single",
        "answers" => [
            0 => "`frac( |y_0 - tilde(a)_(\"ср\")^\"э\"| )(S_e) > t_(beta(f_e))^\"табл\"`",
            1 => "`frac( |y_0 - tilde(a)_(\"ср\")^\"э\"| )(S_e) <= t_(beta(f_e))^\"табл\"`",
            2 => "`frac( |y_(\"ср\")^\"э\" - tilde(a)_0| )(S_e) > t_(beta(f_e))^\"табл\"`",
            3 => "`frac( |y_(\"ср\")^\"э\" - tilde(a)_0| )(S_e) <= t_(beta(f_e))^\"табл\"`",
        ],
        "correct" => 2,
    ],
];