<?php
return [
    "0" => [
        "text" => "Какая функция используется в Matlab для построения графиков функций одной переменной?",
        "type" => "single",
        "answers" => [
            "plot",
            "plot2d",
            "surf",
            "surf3d"
        ],
        "correct" => 0,
    ],
    "1" => [
        "text" => "Какая функция используется в Matlab для построения графиков функций двух переменных?",
        "type" => "single",
        "answers" => [
            "plot",
            "plot2d",
            "surf",
            "surf3d"
        ],
        "correct" => 2
    ],
    "2" => [
        "text" => "Чтобы построить несколько графиков в одной системе координат нужно:",
        "type" => "single",
        "answers" => [
            "Несколько раз выполнить команду plot для каждого набора данных",
            "Выполнить команду plot с несколькими наборами данных в аргументах",
            "Выполнить команду plotall с несколькими наборами данных в аргументах",
            "Несколько раз выполнить команду plotall для каждого набора данных"
        ],
        "correct" => 1
    ],
    "3" => [
        "text" => "Чтобы изменить цвет и формат линни графика нужно:",
        "type" => "single",
        "answers" => [
            "Указать цвет и формат при помощи функции format до выполнения функции plot",
            "Указать цвет и формат при помощи функции format после выполнения функции plot",
            "Указать цвет при помощи функции color, а формат при помощи функции format после выполнения функции plot",
            "Передать цвет и формат третьим аргументом функции plot",
        ],
        "correct" => 3
    ],
    "4" => [
        "text" => "Функция meshgrid в Matlab",
        "type" => "single",
        "answers" => [
            "Возвращает двухмерную сетку координат, построенную по координатам исходных векторов",
            "Возвращает двухмерные сетки координат, построенные по координатам исходных векторов",
            "Возвращает одномерные сетки координат, построенные по координатам исходных векторов",
            "Возвращает одномерную сетку координат, построенную по координатам исходных векторов",
        ],
        "correct" => 1
    ],
    "5" => [
        "text" => "Функция log(x) в Matlab",
        "type" => "single",
        "answers" => [
            "Возвращает значение десятичного логарифма x",
            "Возвращает значение натурального логарифма x",
            "Возвращает значение логистической функции от x",
            "Меняет тип переменной x на boolean",
        ],
        "correct" => 1
    ],
    "6" => [
        "text" => "Как правильно задать шаг 0.5 при построении графика в интервале от -10 до 10?",
        "type" => "single",
        "answers" => [
            "y = 0.5:-10:10",
            "y = -10:0.5:10",
            "y = -10:10:0.5",
            "x = 0.5:-10:10",
            "x = -10:0.5:10",
            "x = -10:10:0.5",
        ],
        "correct" => 4
    ],
    "7" => [
        "text" => "Какой командой можно задать подпись оси X на графике?",
        "type" => "single",
        "answers" => [
            "xlabel('X')",
            "ylabel('X')",
            "xlabel(X)",
            "xlabel(X, 'X')",
        ],
        "correct" => 0
    ],
    "8" => [
        "text" => "Как включить сетку на графике?",
        "type" => "single",
        "answers" => [
            "mesh on;",
            "label on;",
            "grid on;",
            "grid = on;",
            "label = on;",
            "mesh = on;",
        ],
        "correct" => 2
    ],
    "9" => [
        "text" => "Выберите форматную строку для команды plot, задающую черную штрих-пунктирную линию с выколотыми точками",
        "type" => "single",
        "answers" => [
            "k.-",
            "b.-",
            "bo-.",
            "ko-.",
        ],
        "correct" => 3
    ],
    "10" => [
        "text" => "Укажите метод, не являющийся методом одномерной оптимизации",
        "type" => "single",
        "answers" => [
            0 => "Метод деления на три равных отрезка",
            1 => "Метод деления отрезка пополам",
            2 => "Метод золотого сечения",
            3 => "Метод Пауэлла",
        ],
        "correct" => 3,
    ],
    "11" => [
        "text" => "Глобальный экстремум это:",
        "type" => "single",
        "answers" => [
            0 => "Точка, в которой достигается наибольшее (или наименьшее) значение функции при соблюдении дополнительных ограничительных условий",
            1 => "Точка, находящаяся в некоторой окрестности данной точки, в которой достигается наибольшее (или наименьшее) значение функции",
            2 => "Точка, в которой достигается наибольшее (или наименьшее) значение функции на всей области определения функции",
            3 => "Точка из области определения функции, в которой она принимает нулевое значение",
        ],
        "correct" => 2,
    ],
    "12" => [
        "text" => "Локальный экстремум это:",
        "type" => "single",
        "answers" => [
            0 => "Точка, в которой достигается наибольшее (или наименьшее) значение функции при соблюдении дополнительных ограничительных условий",
            1 => "Точка, находящаяся в некоторой окрестности данной точки, в которой достигается наибольшее (или наименьшее) значение функции",
            2 => "Точка, в которой достигается наибольшее (или наименьшее) значение функции на всей области определения функции",
            3 => "Точка из области определения функции, в которой она принимает нулевое значение",
        ],
        "correct" => 1,
    ],
    "13" => [
        "text" => "Какая функция Matlab используется для одномерной оптимизации?",
        "type" => "single",
        "answers" => [
            0 => "fminbnd",
            1 => "fminsearch",
            2 => "fminprog",
            3 => "linprog",
        ],
        "correct" => 0,
    ],
    "14" => [
        "text" => "Задача одномерной оптимизации заключается в том, чтобы",
        "type" => "single",
        "answers" => [
            0 => "Найти максимум или минимум линейной функции многих переменных при линейных ограничениях в виде равенств или неравенств",
            1 => "Найти максимум или минимум линейной функции многих переменных при линейных и нелинейных ограничениях в виде равенств или неравенств",
            2 => "Найти экстремум функции одной переменной",
            3 => "Найти экстремум функции нескольких переменных",
        ],
        "correct" => 2,
    ],
    "15" => [
        "text" => "Укажите численный метод, для которого подходит приведенная иллюстрация<br><img 
src='/img/tasks/Cmath/07-optim/golden.png'>",
        "type" => "single",
        "answers" => [
            0 => "Метод деления на три равных отрезка",
            1 => "Метод золотого сечения",
            2 => "Метод наискорейшего спуска",
            3 => "Симплексный метод",
        ],
        "correct" => 1,
    ],
];