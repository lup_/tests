<?php
require './vendor/autoload.php';

function getLabConfig($labCode) {
    $labConfigs = [
        8 => [
            "name" => "8",
            "type" => "single",
            "exec" => "/var/www/tests/Exec/Optim/08-heat-mix-flow.sh \":vars\" \":variant\"",
            "maxValue" => 9,
            "tasks" => [
                "1" => [
                    "text" => "",
                    "vars" => [
                        "int(10-20)" => ["CX"],
                        "int(1-5)" => ["CF"],
                        "int(5-10)" => ["NU"],
                        "int(450-550)" => ["KT"],
                        "int(4000-5000)" => ["CP"],
                        "int(3000-3500)" => ["CPX"],
                        "float(110-120,1)" => ["T0"],
                        "float(80-90,1)" => ["T"],
                        "float(15-25,1)" => ["T0X"],
                        "float(0.2-0.7,1)" => ["D"],
                    ],
                    "answer" => ["type" => "matlab"],
                ],
            ]
        ],
        9 => [
            "name" => "9",
            "type" => "single",
            "exec" => "/var/www/tests/Exec/Optim/09-heat-flow-flow-cocurrent.sh \":vars\" \":variant\"",
            "maxValue" => 9,
            "tasks" => [
                "1" => [
                    "text" => "",
                    "vars" => [
                        "int(10-20)" => ["CX"],
                        "int(1-5)" => ["CF"],
                        "int(5-10)" => ["NU"],
                        "int(450-550)" => ["KT"],
                        "int(4000-5000)" => ["CP"],
                        "int(3000-3500)" => ["CPX"],
                        "float(110-120,1)" => ["T0"],
                        "float(80-90,1)" => ["T"],
                        "float(15-25,1)" => ["T0X"],
                        "float(0.2-0.7,1)" => ["D"],
                    ],
                    "answer" => ["type" => "matlab"],
                ],
            ]
        ],
        10 => [
            "name" => "10",
            "type" => "single",
            "exec" => "/var/www/tests/Exec/Optim/10-heat-flow-flow-backcurrent.sh \":vars\" \":variant\"",
            "maxValue" => 10,
            "tasks" => [
                "1" => [
                    "text" => "",
                    "vars" => [
                        "int(10-20)" => ["CX"],
                        "int(1-5)" => ["CF"],
                        "int(5-10)" => ["NU"],
                        "int(450-550)" => ["KT"],
                        "int(4000-5000)" => ["CP"],
                        "int(3000-3500)" => ["CPX"],
                        "float(110-120,1)" => ["T0"],
                        "float(80-90,1)" => ["T"],
                        "float(15-25,1)" => ["T0X"],
                        "float(0.2-0.7,1)" => ["D"],
                    ],
                    "answer" => ["type" => "matlab"],
                ],
            ]
        ],
    ];

    return $labConfigs[$labCode];
}

function getLabVars($labCode, $variant) {
    $labs = new \Core\Labs('Optim');
    $labConfig = getLabConfig($labCode);
    $taskCode = 1;
    $taskConfig = $labConfig['tasks'][$taskCode];
    $vars =  $labs->getAllVars($taskConfig['vars'], $taskCode, $variant);

    return $vars;
}

function getLabAnswers($labCode, $variant) {
    $labs = new \Core\Labs('Optim');
    $labConfig = getLabConfig($labCode);
    $answer = $labs->getLabAnswers($labCode, $variant, $labConfig);

    preg_match_all('#([a-zA-Z_]+) *=.*?([0-9.]+)#ms', $answer[0]['text'], $matches);

    $results = [];
    foreach ($matches[1] as $index => $varName) {
        $varValue = $matches[2][$index];
        $results[$varName] = $varValue;
    }

    return $results;
}

$vars = [];
$result = [];

$labNumber = 10;

for ($variant = 1; $variant <= 40; $variant ++) {
    $variantCode = "{$labNumber}-{$variant}";
    $vars[$variant] = getLabVars($labNumber, $variantCode);
    $result[$variant] = getLabAnswers($labNumber, $variantCode);
    echo "$variant\n";
}

$csv = [];
$varNames = array_merge( array_keys($vars[1]), array_keys($result[1]) );
$csv[] = implode(';', $varNames);

foreach ($vars as $variant => $variantVars) {
    $variantResults = $result[$variant];
    $row = array_merge($variantVars, $variantResults);
    $csv[] = implode(';', $row);
}

echo implode("\n", $csv)."\n";