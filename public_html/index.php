<?php
use Slim\Http\Request;
use Slim\Http\Response;

require '../vendor/autoload.php';

$app = new \Slim\App(["settings" => ["displayErrorDetails" => true]]);
$app->add(new \Slim\Middleware\Session([
    'name' => 'student_session',
    'autorefresh' => true,
    'lifetime' => '1 month'
]));

$container = $app->getContainer();
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../Templates', ['autoescape' => false]);

    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

$container['session'] = function () {
    return new \SlimSession\Helper;
};

$app->get('/', function (Request $request, Response $response, array $args) {
    $coursesList = [];
    foreach (\Core\Labs::getCoursesList() as $course) {
        $coursesList[] = [
            "name" => $course['name'],
            "url" => "/{$course['code']}/",
        ];
    }
    return $this->view->render($response, 'index.html', [
        'courses' => $coursesList,
    ]);
});

$app->get('/{course}/', function (Request $request, Response $response, array $args) {
    $courseCode = ucfirst( $args['course'] );
    $labsConfig = new \Core\Labs($courseCode);
    $labList = [];
    $questionList = [];
    $variant = $this->session->get('variant');
    $courseConfig = $labsConfig->getCourseConfig($courseCode);

    foreach ($labsConfig->getLabList() as $labCode => $labConfig) {
        $labList[] = [
            "name" => $labConfig['name'],
            "maxValue" => $labConfig['maxValue'],
            "url" => "/{$courseCode}/{$labCode}/{$variant}/",
        ];

        $questionList[] = [
            "name" => $labConfig['name'],
            "url" => "/questions/{$courseCode}/{$labCode}/",
        ];
    }

    return $this->view->render($response, 'labs.html', [
        "labs" => $labList,
        "questions" => $questionList,
        "files" => $courseConfig['files'],
        "gradebooks" => $courseConfig['gradebooks'],
        "variant" => $variant,
        "course" => $courseCode,
        "feedbackUrl" => $courseConfig['feedbackUrl'],
        "interactive" => $courseConfig['interactive'],
    ]);
});

$app->get('/questions/{course}/{lab}/', function (Request $request, Response $response, array $args) {
    $courseCode = ucfirst( $args['course'] );

    $labs = new \Core\Labs($courseCode);
    $labConfig = $labs->getLabConfig($args['lab']);

    $questions = new \Core\Questions($courseCode);
    $questionList = $questions->getList($args['lab']);

    return $this->view->render($response, 'questions.html', [
        "lab" => $labConfig['name'],
        "labCode" => $args['lab'],
        "course" => $courseCode,
        'questions' => $questionList,
    ]);
});

$app->post('/questions/{course}/{lab}/', function (Request $request, Response $response, array $args) {
    $courseCode = ucfirst( $args['course'] );

    $labs = new \Core\Labs($courseCode);
    $labConfig = $labs->getLabConfig($args['lab']);

    $questions = new \Core\Questions($courseCode);
    $answers = $request->getParam('question');
    $labQuestions = $questions->getList($args['lab']);
    $result = $questions->checkAnswers($labQuestions, $answers);


    return $this->view->render($response, 'questionResult.html', [
        "lab" => $labConfig['name'],
        "labCode" => $args['lab'],
        "course" => $courseCode,
        'result' => $result,
    ]);
});

$app->get('/{course}/{lab}/{variant}/', function (Request $request, Response $response, array $args) {
    $courseCode = ucfirst( $args['course'] );
    $labs = new \Core\Labs($courseCode);
    $labsConfigs = $labs->getLabList();
    $labConfig = $labsConfigs[$args['lab']];
    $taskList = $labs->getLabTasks($args['lab'], $args['variant']);

    return $this->view->render($response, 'lab.html', [
        "variant" => $args['variant'],
        "lab" => $labConfig['name'],
        "labCode" => $args['lab'],
        "isMultiple" => $labConfig['type'] == 'multiple',
        "tasks" => $taskList,
        "course" => $courseCode,
    ]);
});

$app->get('/{course}/{lab}/{variant}/answers/', function (Request $request, Response $response, array $args) {
    set_time_limit(120);
    $courseCode = ucfirst( $args['course'] );
    $labs = new \Core\Labs($courseCode);
    $labConfig = $labs->getLabConfig($args['lab']);
    $answersList = $labs->getLabAnswers($args['lab'], $args['variant']);

    return $this->view->render($response, 'labans.html', [
        "variant" => $args['variant'],
        "lab" => $labConfig['name'],
        "labCode" => $args['lab'],
        "isMultiple" => $labConfig['type'] == 'multiple',
        "answers" => $answersList,
        "course" => $courseCode,
    ]);
});

$app->get('/setVariant/{course}/', function (Request $request, Response $response, array $args) {
    $variant = $request->getParam('groupCode').'-'.$request->getParam('studentCode');
    $this->session->set('variant', $variant);
    return $response->withRedirect("/{$args['course']}/");
});


$app->get('/exam/{course}/', function (Request $request, Response $response, array $args) {
    $courseCode = ucfirst( $args['course'] );
    $examQuestionsCount = 40;

    $questions = new \Core\Questions($courseCode);
    $questionList = $questions->getRandomList($examQuestionsCount);

    return $this->view->render($response, 'questions.html', [
        "isExam" => true,
        "course" => $courseCode,
        "questions" => $questionList,
        "questionCodes" => array_keys($questionList),
    ]);
});

$app->post('/exam/{course}/', function (Request $request, Response $response, array $args) {
    $courseCode = ucfirst( $args['course'] );

    $questions = new \Core\Questions($courseCode);

    $questionCodes = $request->getParam('questionCode');
    $allQuestions = $questions->getTotalList();
    $examQuestions = [];
    foreach ($questionCodes as $code) {
        $examQuestions[] = $allQuestions[$code];
    }

    $answers = $request->getParam('question');
    $result = $questions->checkAnswers($examQuestions, $answers);

    return $this->view->render($response, 'questionResult.html', [
        "isExam" => true,
        "course" => $courseCode,
        'result' => $result,
    ]);
});




$app->run();